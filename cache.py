class DiffCache :

    def __init__(self):
        self.cache = {}        

    def get(self,key):
        return self.cache[key]

    def contains(self,key):        
        return key in self.cache

    def add(self,key,val):
        self.cache[key] = val

    def make_space(self):
        pass
