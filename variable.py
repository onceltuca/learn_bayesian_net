from cache import *
import copy

class Variable:
    def __init__(self,id,r):
        self.id = id
        self.range = r
        self.fam_score = 0
        self.counts = {}
        self.cpd = None
        self.attr = {}
    # Need to know the range of the parents here
    def generate_cpd(self):
        pass

    def tostring(self):return "node "+str(self.id)

    def __str__(self):
        #return "Reference: " + str(id(self))+" Id: "+str(self.id)#+","+ "cpd: \n"+str(self.cpd)
        return str(self.id)

    def __repr__(self):
        return self.__str__()

    def copy(self):
        ret = Variable(self.id)
        ret.range = self.range
        ret.fam_score = self.fam_score
        ret.counts = self.counts
        ret.cpd = self.cpd
        ret.attr = self.attr
        return ret
