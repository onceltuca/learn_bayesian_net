import numpy as np
## Testing at first
## using the BN of Figure 3.4 in Probabilistic Graphical Models

## P(D)
difficulty = np.array([0.6, 0.4]) 
## P(I)
intelligence = np.array([0.7, 0.3] )
## P(G|D,I)
grade = np.array([[0.3, 0.4, 0.3],
                  [0.05, 0.25, 0.7],
                  [0.9, 0.08, 0.02],
                  [0.5, 0.3, 0.2]])
## P(S|I)
sat = np.array([[0.95, 0.05],
                [0.2, 0.8]])
## P(L|G)
letter = np.array([[0.1, 0.9],
                   [0.4, 0.6],
                   [0.99, 0.01]])
#p = P(L=1)
p = np.dot(grade,letter)

p[0] = p[0]*difficulty[0]*intelligence[0]
p[1] = p[1]*difficulty[0]*intelligence[1]
p[2] = p[2]*difficulty[1]*intelligence[0]
p[3] = p[3]*difficulty[1]*intelligence[1]

print p
print sum(p)
