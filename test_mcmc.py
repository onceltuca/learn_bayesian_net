import unittest
from chinese_rest import * 
from mcmc import  *
import numpy as np
import bayesian_network as bn
import networkx as nx
from ploting import *
import random
# THIS IS ESSENTIAL FOR THE TESTS
random.seed(1)
np.random.seed(1)
state = random.getstate()
random.setstate(state)

class TestMCMC(unittest.TestCase):
    @unittest.skip("No real test")
    def test_transition_ratio(self):
        n = 8
        # First rest graph
        r1 = Restaurant(1,n,0)
        r1.set_state([[6,7],[0,1,2],[5],[3,4]])
        nodes1 = [bn.Variable(i,2) for i in range(n)]
        net1 = bn.BayesianNetwork(nodes1,[(0,5),(1,2),(3,5)],[])
        rg1 = RestGraph(r,1,1)
        rg1.set_network(net1)

        # Second rest graph
        r2 = Restaurant(1,n,0)
        r2.set_state([[6,7],[0,1,2],[5],[3,4]])
        nodes2 = [bn.Variable(i,2) for i in range(n)]
        net2 = bn.BayesianNetwork(nodes2,[(0,5),(1,2),(3,5)],[])        
        rg2 = RestGraph(r,1,1)
        rg2.set_network(net2)
        print rg1

    def test_trans_cluster(self):
        n = 5
        alpha = 1
        r1 = Restaurant(alpha, n, 0)
        r1.set_state([[0],[1],[2],[3],[4]])

        r2 = Restaurant(alpha, n, 0)
        r2.set_state([[0],[1,2],[3],[4]])
        
        ki = 1
        kj = 1
        expect = (ki * kj) / np.float((n*(alpha +n -1 )))

        ret = get_transition_rest(r1,r2,1)
        self.assertAlmostEqual(ret, expect)

        ki = 2
        K2 = 5
        expect = (ki * alpha) / np.float( n * (alpha + n - 1) * K2 )
        ret = get_transition_rest(r2,r1,1)
        self.assertAlmostEqual(ret, expect)

        ret = get_transition_rest(r2,r2,1)
        expect = (2.0/5.0)*(1.0/3.0) + 3.0* (1.0/5.0)*(1.0/5.0)*(1.0/4.0)

        self.assertAlmostEqual(ret, expect)

        ret = get_graph_trans(r2, 1)
        print "graph trans"
        print ret