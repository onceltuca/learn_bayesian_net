from rest_graph import *
import numpy as np
import matplotlib.pyplot as plt
import pygraphviz
import networkx as nx
from pylab import *
from matplotlib import rc


rcParams['figure.figsize'] = 15, 10

rc('font', **{'family':'sans-serif', 'sans-serif': ['Helvetica']})
rc('text', usetex=True)


def print_mean_graph(restaurants):
    graphs = [r.network for r in restaurants]
    print get_means_graph(graphs, "settings", restaurants[0].no_persons)


def print_rest_chain(restaurants):
    for i in len(restaurants):
        net = restaurants[i].network.get_networkx()
        print nx.to_numpy_matrix(net, range(n))
        print "("+str(i)+") "+ str(restaurants[i].prob())


def count_graphs(graphs):
    """
    Input: graphs: ordered by probability of their restgraph
    """
    graph_counts = {}
    for iz, network in enumerate(graphs):
        tmp = network.get_tuple()
        if tmp in graph_counts:
            graph_counts[tmp] += 1
        else:
            graph_counts[tmp] = 1
    return graph_counts


def count_restgraphs(restgraphs, burnin):
    """
    Input: graphs: ordered by probability of their restgraph
    """
    ret = []
    chains = len(restgraphs)
    # Adds every restgraphs to a sicgle list.
    for i in range(chains):
        ret += restgraphs[i][burnin:]

    restgraph_counts = {}
    for iz,rg in enumerate(ret):
        if rg in restgraph_counts :
            restgraph_counts[rg] += 1
        else:
            restgraph_counts[rg] = 1
    return restgraph_counts


def get_best_restgraphs(restgraphs, burnin):
    """ Sorts all restgraphs after its corresonding score.
        The graphs are count from burning and forward.
    """
    ret = []
    chains = len(restgraphs)
    # Adds every restgraphs to a sicgle list.
    for i in range(chains):
        ret += restgraphs[i][burnin:]
    # Sorts so that the rest graphs with highest prob is first
    ret = sorted(ret, key=lambda rg: rg.prob(), reverse=True)
    return ret


def get_same_table_matrix(restaurants):
    n = restaurants[0].no_persons
    matrix = np.zeros(n*n).reshape(n, n)
    for r in restaurants:
        matrix += r.get_partition_matrix()
    matrix /= len(restaurants)
    return matrix


# Create "mean-graph"
def get_means_graph(graphs):
    no_graphs = 0
    # Get number of nodes

    tmp = graphs.keys()[0]
    no_nodes = int(np.sqrt(len(tmp)))
    # pgv = pygraphviz.AGraph(g)
    # no_nodes = pgv.number_of_nodes()

    mean_graph_matrix = np.zeros(no_nodes*no_nodes, dtype=float)
    mean_graph_matrix.shape = (no_nodes, no_nodes)
    order = [str(i) for i in range(no_nodes)]
    for graph_string, count in graphs.iteritems():
        tmp = graph_string #tuple
        tmp =  list(tmp)
        matrix = np.matrix(tmp)
        matrix.shape = (no_nodes,no_nodes)

        #pgv = pygraphviz.AGraph(graph_string)
        #ntx = nx.from_agraph(pgv)
        #matrix = nx.to_numpy_matrix(ntx,order)

        mean_graph_matrix += matrix * count
        no_graphs += count

    mean_graph_matrix /= no_graphs
    return mean_graph_matrix

def plot_heat_map(data,path,filename,title):
    size = data.shape[0]
    column_labels = range(data.shape[0])
    row_labels = range(data.shape[1])
    data = data.view(np.ndarray)
    data.shape = (size,size)
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.rc('font', size=20)
    plt.subplots_adjust(top=0.8)
    fig, ax = plt.subplots()
    heatmap = ax.pcolor(data, cmap=plt.cm.Greys)
    # put the major ticks at the middle of each cell
    ax.set_xticks(np.arange(data.shape[0])+0.5, minor=False)
    ax.set_yticks(np.arange(data.shape[1])+0.5, minor=False)
    # want a more natural, table-like display
    ax.invert_yaxis()
    #ax.xaxis.tick_top()
    ax.set_xticklabels(row_labels, minor=False)
    ax.set_yticklabels(column_labels, minor=False)
    plt.title(title)
    #plt.xlabel(r"Nodes")
    #plt.ylabel(r"Nodes")
    #plt.show()
    plt.savefig(path+filename+".eps",format="eps",dpi=1000)
    plt.clf()
    # Get the total numer of edges for all simulated graphs
def get_total_number_of_edges(graphs,no_nodes):
    no_edges = np.zeros(no_nodes*no_nodes)
    order = [str(i) for i in range(no_nodes)]
    for dotstring, number in graphs.iteritems() :
        pgv = pygraphviz.AGraph(dotstring)
        ntx = nx.from_agraph(pgv)
        matrix = nx.to_numpy_matrix(ntx,order)
        no_edges[int(matrix.sum())] = no_edges[int(matrix.sum())]+number
    return no_edges

    ##Plot the n most frequantly appearing DAGs
def plot_most_freq_graphs(graphs,path, n, settings_string):
    # TODO: Sort on score instead
    # sorted_graphs = sorted(graphs.iteritems(),
    #                       key=operator.itemgetter(1))
    for i,v in enumerate(reversed(sorted_graphs)):
        #tmp = pygraphviz.AGraph(v[0])
        tmp2 = list(v[0])
        m = int(np.sqrt(len(tmp2)))
        matrix = np.matrix(tmp2)
        matrix.shape = (m,m)
        plot_heat_map(matrix,path,settings_string+"graph_"+str(i+1)+"-"+str(v[1]), i)

        # agraph.layout()
        # agraph.draw(path+settings_string+"graph_"+str(i)+"-"+str(v[1])+".eps", format='eps',dpi=1000)
        if i > n: break

def plot_log_scores(restaurants,path,filename, title, prob_string , true_score):
    plt.subplots_adjust(top=0.80)
    x = range(len(restaurants))
    scores = [r.prob() for r in restaurants]
    plt.plot(x, [true_score]*len(x) )
    plt.plot(x, scores)
    plt.title("\n"+title)
    plt.ylabel(r"$\log {P(d^{(t)}, \underline z^{(t)} | \mathbf x)}$")
    plt.xlabel(r"$t$")
    #plt.show()
    plt.savefig(path+filename, format='eps',dpi=1000)


# def get_person_tables_matrix(restaurants):
#     n =  restaurants[0].no_persons
#     matrix = np.zeros(n*n).reshape(n,n)
#     for r in restaurants :
#         for p in r.persons:
#             matrix[p.id][p.table.id] += 1
#     matrix /= len(restaurants)
#     return matrix

# def get_person_order_matrix(restaurants):
#     n =  restaurants[0].no_persons
#     matrix = np.zeros(n*n).reshape(n,n)
#     for r in restaurants:
#         for p in r.persons:
#             matrix[p.id][p.table.order] += 1
#     matrix /= len(restaurants)
#     return matrix
