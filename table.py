class Table:
    """ Table class in the CRP    
    """
    def __init__(self,id, person):
        """ Constructor
            Id is actually totally uniteresting. It is just an 
            implementation thing.
        """
        self.id = id
        self.order = id
        self.persons = [] ## make this to id list
        person.table = self
        self.add_person(person) 

    def is_empty(self): return len(self.persons) == 0
    def add_person(self,p):
        p.table = self # new
        self.persons.append(p.id)
        self.persons.sort()
            
    def remove_person(self,p):
        self.persons.remove(p.id)
            
    def __str__(self):
        s = "Table("+str(id(self))+") Id: "+str(self.id)+", Order: "+str(self.order)+", persons: "+str(self.persons)
        return s
    
    def __repr__(self):
        return self.__str__()
        
    def copy(self):    
        pass
