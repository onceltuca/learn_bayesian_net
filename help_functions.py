import networkx as nx
import math
import scipy.misc
from sets import Set
import numpy as np
from chinese_rest import *
import chinese_rest
from rest_graph import *
import variable as bnvar
import bayesian_network as bn
import json

def simulate_restgraph(n,alpha,b1,b2,support,mode):
    """ Simulate network and structure """
    rest = chinese_rest.Restaurant(alpha, n, 666)
    rg = RestGraph(rest, b1, b2, mode)
    rest.assign_persons()
    rest.shuffle_tables()
    rg.set_edge_prob(mode) 
    rg.create_network(None, support) 
    return rg

def sim_cpds(bn, gamma):
    bn.generate_cpds(gamma)
    cpds = [n.cpd for n in bn.nodes]
    return cpds

def set_cpds(bn, cpds):
    bn.generate_cpds(gamma)
    for i,n in enumerate(bn.nodes):
        n.cpd = cpds[i]

def read_tables(file):
    z = np.loadtxt(file, skiprows=0,dtype=int)
    return tuple(z[0])
        
def read_networks(file):
    """ Read network from file """
    tmp = np.loadtxt(file, skiprows=0,dtype=int)
    d = tmp
    n = int(np.sqrt(len(d)))    
    dag = nx.DiGraph(np.matrix(d).reshape(n,n))
    return dag

def read_network_json(filename):
    tmp = open(filename).read()     
    dag_json = json.loads(tmp)
    dag = nx.DiGraph()
    for i in range(dag_json["nodes"]): dag.add_node(i)
    for e in dag_json["edges"]: dag.add_edge(e[0],e[1])
    return dag

def read_bn_json(filename, support):
    dag = read_network_json(filename)
    n = len(dag)
    nodes = [ bnvar.Variable(i,support[i]) for i in range(n) ]
    bnet = bn.BayesianNetwork()
    bnet.set_nodes(nodes)
    for e in dag.edges():
        bnet.add_edge(e)
    return bnet

def read_bn(filename,support):
    """ read structure """
    dag =  read_networks(filename)
    n =  len(dag)

    nodes = [ bnvar.Variable(i,support[i]) for i in range(n) ]
    bnet = bn.BayesianNetwork()
    bnet.set_nodes(nodes)
    for e in dag.edges():
        bnet.add_edge(e)
    return bnet

def read_cpds(filename,bnet):
    """ set cpds """
    cpds_json = open(filename).read()     
    cpds = json.loads(cpds_json)
    for i,node in enumerate(bnet.nodes):
        node.cpd = np.array(cpds["cpds"][i])

def read_z(filename,alpha):
    """ read z """
    z = np.loadtxt(filename, skiprows=0,dtype=int)
    n = len(z)
    K = max(z)+1
    tables = [[] for i in range(K)]
    for i,v in enumerate(z):
        tables[v].append(i)
    rest = chinese_rest.Restaurant(alpha,n,0)
    rest.set_state(tables)
    return rest


def tpr(true_graph, est_graph):
    """ Takes 2 adjecency matrices
    """
    N = len(true_graph)
    no_correct = 0.0
    no_false_rej = 0.0

    for i in range(N):
        for j in range(N):
            if est_graph.item(i,j) == 1 and true_graph.item(i,j) == 1:
                    no_correct += 1

            if true_graph.item(i, j) == 1:
                if est_graph.item(i,j) == 0:
                    no_false_rej += 1
    return no_correct / (no_correct + no_false_rej)

def spc1(true_graph, est_graph):
    """ Takes 2 adjecency matrices
    """
    N = len(true_graph)
    no_corr_rej = 0.0
    no_wrong_incl = 0.0

    for i in range(N):
        for j in range(N):
            if est_graph.item(i,j) == 1 and true_graph.item(i,j) == 0:
                no_wrong_incl += 1
            if est_graph.item(i,j) == 0 and true_graph.item(i,j) == 0:
                no_corr_rej += 1

    return no_corr_rej / (no_corr_rej + no_wrong_incl)

def spc2(true_graph, est_graph):
    """ Takes 2 adjecency matrices
    """
    N = len(true_graph)
    no_in_skeleton = 0.0
    no_wrong_incl = 0.0


    skeleton = np.zeros(N*N, dtype = int)
    skeleton.shape = (N,N)
        
    for i in range(N):
        for j in range(N):
            if est_graph.item(i,j) == 1:
                skeleton[i][j] = 1
                skeleton[j][i] = 1
                
    for i in range(N):
        for j in range(i+1,N):
            if skeleton[i][j] == 1:
                no_in_skeleton += 1 

    for i in range(N):
        for j in range(N):
            if est_graph.item(i,j) == 1 and true_graph.item(i,j) == 0:
                no_wrong_incl += 1

    if no_in_skeleton + no_wrong_incl == 0: return 1
    return no_in_skeleton / (no_in_skeleton + no_wrong_incl)

def dag_diameter(dag):
    diameter = 0
    if nx.is_connected(dag.to_undirected()) == False:
        return 0
    print dag.edges()
    for n in dag:
        #if len(dag.predecessors(n)) == 0:
        print "node" + str(n)
        print "pred " +str(dag.predecessors(n))
        print "succ "+str(dag.successors(n))
        if len(dag.predecessors(n)) == 0:
            print n
            if nx.eccentricity(dag,n) > diameter:
                diameter = nx.eccentricity(dag,n)

    return diameter

def moralize(dag):
    g = dag.to_undirected()
    for n in nx.nodes(dag):
        for p1 in dag.predecessors(n):
            for p2 in dag.predecessors(n):
                if not p1 == p2:
                    g.add_edge(p1,p2)
    return g

def get_immoralities(dag):
    g = dag.to_undirected()
    immoralities = Set()
    for n in nx.nodes(dag):
        for p1 in dag.predecessors(n):
            for p2 in dag.predecessors(n):
                if not p1 == p2:
                    if not (dag.has_edge(p1, p2) or dag.has_edge(p2, p1)) :
                        immoralities.add(Set([p1,n,p2]))        
    return immoralities

def count_partial(partial_orders, n):
    nodes =  n
    dag = nx.DiGraph()
    dag.add_edges_from(partial_orders)
    dag.add_nodes_from(range(n))
    return count(dag)
    

def count(dag):
    graph = dag.to_undirected()
    if nx.is_connected(graph):
        # Is connected so find static sets.
        static_sets = get_static_sets(dag)
        if static_sets == None :
            return orders(dag)
        else :
            ret = 1
            for static_set in static_sets:                
                ret *= count(dag.subgraph(static_set))
            return ret
    else:
        # Get weakly connected components
        comp = []
        components = nx.weakly_connected_components(dag)
        for c in components:
            comp.append(dag.subgraph(c))
            comp_orders = []
        # Binomial coefficient
        bincoeff = math.factorial(len(dag))
        for c in components:
            bincoeff /= math.factorial(len(c))            
        ret = bincoeff
        # Multiply the orders of the connected components
        for g in comp:
            ret  *= count(g)

    return ret

class Element:
    def __init__(self,node,lower,upper):        
        self.lower = lower
        self.upper = upper
        self.node = node
    def __str__(self):
        return "node:"+str(self.node)+" lower:"+str(self.lower)+" upper:"+str(self.upper)
    def __repr__(self):
        return self.__str__()

def get_static_sets(dag):
    dag_rev = dag.reverse(copy=True)
    array = []
    for n in dag :     
        lower = len(nx.descendants(dag_rev,n)) + 1        
        upper = len(dag) - len(nx.descendants(dag,n))
        e = Element(n, lower, upper)
        array.append(e)
    # sort
    array.sort(key=lambda x: x.upper,reverse=True)
    array.sort(key=lambda x: x.lower,reverse=False)
    i = 0
    count = 0
    l = array[i].lower
    u = array[i].upper
    span = u - l + 1
    last = l
    if span == len(dag) : return None
    
    S = [] # Static sets
    P = [] # Set with nodes
    
    while True :
        P.append(array[i].node)
        count += 1
        if array[i].lower > last :
            if array[i].upper > u :
                u = array[i].upper
                span = u - l + 1
                if span == len(dag): return None
            last = array[i].lower
        i += 1
        if count == span :
            S.append(P)
            if i < len(dag):
                l = array[i].lower
                u = array[i].upper
                span = u - l + 1
                count = 0
                P = []
        if i >= len(dag) : break # Do-while loop
    return S
        
""" Number of topological orders.
"""       
def orders_rel(partial_orders,n):
    nodes =  n
    dag = nx.DiGraph()
    dag.add_edges_from(partial_orders)
    dag.add_nodes_from(range(n))
    return orders(dag)

def get_roots(dag):        
    roots = []
    for node in dag:
        if dag.predecessors(node) == []:            
            roots += [node] 
    return roots
    
def orders(dag):
    if len(dag) == 1:
        return 1

    roots = get_roots(dag)
    # First copy the graph. Then remove the unwanted node.
    # Maybe not that constructive.
    dags = [ dag.copy() for i in range(len(roots)) ]

    # Remove the root in each graph.    
    for i,root in enumerate(roots):
        dags[i].remove_node(root)

    ret = 0
    for dag in dags :
        ret += orders(dag)    
    
    
    return ret
