import unittest
from chinese_rest import * 
import numpy as np
import bayesian_network as bn
import networkx as nx
import random
# THIS IS ESSENTIAL FOR THE TESTS
random.seed(1)
np.random.seed(1)
state = random.getstate()
random.setstate(state)

class TestCRP(unittest.TestCase):
    """ Do not test things tha
        assertAlmostEqual(a, b)	round(a-b, 7) == 0	 
        assertNotAlmostEqual(a, b)	round(a-b, 7) != 0	 
        assertGreater(a, b)	a > b	2.7
        assertGreaterEqual(a, b)	a >= b	2.7
        assertLess(a, b)	a < b	2.7
        assertLessEqual(a, b)	a <= b	2.7
        assertRegexpMatches(s, r)	r.search(s)	2.7
        assertNotRegexpMatches(s, r)	not r.search(s)	2.7
        assertItemsEqual(a, b)	sorted(a) == sorted(b) and works with unhashable objs	2.7
        assertDictContainsSubset(a, b)	all the key/value pairs in a exist in b	2.7t cannot be tested.
        
    """
    def setUp(self):
        self.r = Restaurant(3,5,0)
        self.r.assign_persons()
    @unittest.skip("No real test")
    def test_shuffle_tables(self):
        r = self.r.copy()
        #print r.tables
        r.shuffle_tables()
        #print r.tables
        r.shuffle_tables()
        #print r.tables
        r.shuffle_tables()
        #print r.tables
    @unittest.skip("No real test")
    def test_higher_order(self):
        r = self.r.copy()
        #print r
        p = r.persons[2]
        #print "number of higher orders than perosn"+str(p)
        #print r.get_number_of_persons_of_higher_order(p)

    def test_remove_person(self):
        r = self.r.copy()
        pid = 1
        p = r.persons[pid]
        table = p.table 
        r.leave(p)
        #print r.tables[table.id].persons
        #print table.persons
        self.assertTrue(p.id not in r.tables[table.id].persons)
        self.assertTrue(p.id not in table.persons)
        
        pid = 2
        p = r.persons[pid]
        table = p.table 
        r.leave(p)

        self.assertTrue(p.id not in r.tables[table.id].persons)
        self.assertTrue(p.id not in table.persons)

    
    def test_new_crp(self):
        r = Restaurant(3,8,0)
        r.add_persons(20)
        r.del_person(4)
        r.del_person(7)
        r.add_person()
        self.assertEqual(19,r.no_pers())
        self.assertEqual(19,sum(r.m))

    @unittest.skip("No real test")
    def test_reorder_table(self):
        r = Restaurant(1,8,0)
        r.set_state([[6,7],[0,1,2],[5],[3,4]])
        #print r

        t = r.tables[1]
        # Move up
        r.reorder_table(t,2)
        self.assertTrue(r.tables[1].order == 2)
        #print r

        r.set_state([[6,7],[0,1,2],[5],[3,4]])
        t = r.tables[3]
        # Move down
        r.reorder_table(t,1)
        self.assertTrue(r.tables[3].order == 1)
        #print r
        

    def test_partition_matrix(self):
        alpha = 1
        r1 = Restaurant(alpha,3,0)
        r1.set_state([[0],[1],[2]])

        r2 = Restaurant(alpha,3,0)
        r2.set_state([[0],[2],[1]])
        print r1.get_partition_matrix()
        print r2.get_partition_matrix()
        self.assertTrue(r1 == r2)
        self.assertEqual(r1,r2)

    def test_prob_person_assignment_standard_perm(self):
        alpha = 1
        r = Restaurant(alpha,3,0)
        r.set_state([[0,1,2]])
        true = (1.0/3.0)
        #print true
        #print np.exp(r.prob_person_assignment_standard_perm())
        self.assertTrue( np.allclose(r.prob_person_assignment_standard_perm(), np.log(true)))

    def test_prob_person_assignment_marginal_perm(self):
        alpha = 1
        r = Restaurant(alpha,3,0)
        r.set_state([[0,1,2]])
        true = (1.0/3.0)
        #print true
        #print np.exp(r.prob_person_assignment_marginal_perm())
        self.assertTrue( np.allclose(r.prob_person_assignment_marginal_perm(), np.log(true)))

    def test_assign_persons(self):
        persons = 28
        r = Restaurant(3,persons,0)
        r.assign_persons()
        #r.shuffle_persons()
        #print r
        #print "shuffle tables"
        r.shuffle_tables()
        for p in r.persons:
            assert p.table.id == r.tables[p.table.id].id
    
    @unittest.skip("No real test")
    def test_probs(self):
        alpha = 5

        r = Restaurant(alpha,3,0)
        state = [[0,1,2]]
        r.set_state(state)        
        #print "State: "+str(state)
        #print "Order: "+str(r.prob_order_given_setting())
        #print "Z: " +str(r.prob_person_assignment())

        state = [[0],[1],[2]]
        r.set_state(state)
        #print "State: "+str(state)
        #print "Order: "+str(r.prob_order_given_setting())
        #print "Z: " +str(r.prob_person_assignment())

        alpha = 0.5

        r = Restaurant(alpha,2,0)
        state = [[0,1]]
        r.set_state(state)        
        #print "State: "+str(state)
        #print "Order: "+str(r.prob_order_given_setting())
        #print "Z: " +str(r.prob_person_assignment())

        state = [[0],[1]]
        r.set_state(state)        
        #print "State: "+str(state)
        #print "Order: "+str(r.prob_order_given_setting())
        #print "Z: " +str(r.prob_person_assignment())
