# A family is a sorted tuple of tuples indicating ..
# Ex.
# Family = (3,(4,6,8,23))
import numpy as np
import scipy.special as ss
import networkx as nx
import copy
import ploting
import json


class BayesianNetwork:

    # Edges are added afterwards
    def __init__(self):
        """ Edges is a list of ints indicating the variables index,
        """
        self.diff_cache = DiffCache()
        self.cooper_likelihood = 0
        self.dag = nx.DiGraph()
        self.data = None

    def __str__(self):
        ret = "BN Ref: "+str(id(self))
        ret += " Nodes: "+str(self.dag.number_of_nodes())
        ret += ", Edges: "+str(self.dag.edges())
        ret += "\n" + str(self.get_numpy_matrix())
        return ret

    def copy(self):
        ret = BayesianNetwork()
        ret.dag = copy.deepcopy(self.dag)
        ret.nodes = ret.dag.nodes()
        ret.nodes.sort(key=lambda x: x.id, reverse=False)
        ret.diff_cache = self.diff_cache  # Copies reference
        ret.cooper_likelihood = self.cooper_likelihood
        ret.data = self.data  # Copies reference
        ret.prior = self.prior
        ret.gamma = self.gamma
        return ret

    def set_class(self, classes):
        class_dic = {}
        for node in self.get_nodes():
            class_dic[node] = classes[node.id]
        nx.set_node_attributes(self.dag, 'class', class_dic)

    def color_nodes(self, classes):
        """ Takes a list/tuple of classes. E.g. (1,1,0,2) means
            node 0 has color 1 etc.
        """
        colors = ['black', 'red', 'blue',
                  'green', 'grey', 'cyan', 'yellow', 'orange', 'brown']
        col_dic = {}

        for i, node in enumerate(self.get_nodes()):
            col_dic[node] = colors[classes[node.id] % len(colors)]
        nx.set_node_attributes(self.dag, 'color', col_dic)

    def set_nodes(self, nodes):
        for node in nodes:
            self.dag.add_node(node, id=node.id)
        self.nodes = nodes
        self.nodes.sort(key=lambda x: x.id, reverse=False)

    def get_nodes(self):
        return self.nodes

    def set_data(self, data, prior, gamma):
        self.data = data
        self.prior = prior
        self.gamma = float(gamma)
        self.cooper_likelihood = self.cooper_herzkovitz(data, prior, gamma)

    def simulate(self, n):
        data = np.zeros(n*self.dag.number_of_nodes(), dtype=int)
        data -= 1000
        data.shape = (n, self.dag.number_of_nodes())
        sorted_nodes = nx.topological_sort(self.dag)

        for i in range(n):
            for node in sorted_nodes:
                if self.dag.predecessors(node) == []:
                    np.random.multinomial(1, node.cpd).tolist().index(1)
                    data[i][node.id] = np.random.multinomial(1,
                                                             node.cpd).tolist().index(1)
                else:
                    pred_index = [p.id for p in self.dag.predecessors(node)]
                    pred_index.sort()                     # need to be sorted
                    cond_data = data[i][pred_index]
                    data[i][node.id] = np.random.multinomial(1,
                                                             node.cpd[tuple(cond_data)]).tolist().index(1)
        return data

    def generate_cpds(self, gamma=1):
        for n in self.dag.nodes():
            # number of configurations
            configurations = n.range
            shape = tuple()

            pred = self.dag.predecessors(n)
            # print "pred "+str(pred)
            pred.sort(key=lambda x: x.id)
            # print "pred "+str(pred)

            # Potential BUG! need to sort?
            for p in pred:
                configurations *= p.range
                shape += (p.range,)
            shape += (n.range,)
            n.cpd = np.zeros(configurations)
            norm_shape = (configurations/n.range, n.range)
            n.cpd.shape = norm_shape

            for i in range(len(n.cpd)):
                n.cpd[i] = np.random.dirichlet([gamma]*n.range)

            n.cpd.shape = shape
            # print n.cpd
            # print type(n.cpd)

    def estimate_cpds(self, data, gamma=1.0):
        """ Much is copied of generate_cpds
        """
        # First assume no observations
        for n in self.nodes:
            configurations = n.range
            shape = tuple()
            for p in self.dag.predecessors(n):
                configurations *= p.range
                shape += (p.range,)
            shape += (n.range,)

            n.cpd = np.zeros(configurations)
            norm_shape = (configurations/n.range, n.range)
            n.cpd.shape = norm_shape

            for i in range(len(n.cpd)):
                # This is changed from genrerate_cpd
                n.cpd[i] = [gamma]*n.range
                n.cpd[i] /= (n.range * gamma)
            n.cpd.shape = shape

        # No overwrite some of the cpds
        # Go through the counts
        for n in self.dag.nodes():
            self.count_node(data, n)
            for conf, counts in n.counts.items():
                n.cpd[conf] = counts.copy()
                n.cpd[conf] += gamma
                n.cpd[conf] /= (gamma*len(n.cpd[conf]) + counts.sum())
            self.count_node(data, n)

    def has_edge(self, edge):
        nodes = self.nodes
        return self.dag.has_edge(nodes[edge[0]], nodes[edge[1]])

    def add_edges(self, edges):
        if edges == []:
            return
        for edge in edges:
            self.add_edge(edge)

    def del_edges(self, edges):
        for edge in edges:
            self.del_edge(edge)

    def clear_edges(self):
        edges = [(e[0].id, e[1].id) for e in self.dag.edges()]
        self.del_edges(edges)

    def reverse_edges(self, edges):
        """ Reverse all edges in a list
            Args:
                edges: [(from,to),(from,to),...]
        """
        for (fr, to) in edges:
            self.del_edge((fr, to))
            self.add_edge((to, fr))

    def add_edge(self, edge):
        """ Adds an edge.
        """
        fr = edge[0]
        to = edge[1]
        nodes = self.nodes

        tmp = [pre.id for pre in self.dag.predecessors(nodes[to])]
        tmp.sort()
        old_parent_ids = tuple(tmp)

        tmp.append(fr)
        tmp.sort()
        new_parent_ids = tuple(tmp)
        diff_prob = 0
        diff = (to, old_parent_ids, new_parent_ids)
        rev_diff = (to, new_parent_ids, old_parent_ids)

        if self.data is None:
            self.dag.add_edge(nodes[fr], nodes[to])
            return

        if self.diff_cache.contains(diff):
            diff_prob = self.diff_cache.get(diff)
            self.dag.add_edge(nodes[fr], nodes[to])
        elif self.diff_cache.contains(rev_diff):
            diff_prob = -self.diff_cache.get(rev_diff)
            self.dag.add_edge(nodes[fr], nodes[to])
        else:
            old_famscore = self.calc_fam_score(nodes[to],
                                               self.data,
                                               self.prior,
                                               self.gamma)
            self.dag.add_edge(nodes[fr], nodes[to])
            fam_score = self.calc_fam_score(nodes[to],
                                            self.data,
                                            self.prior,
                                            self.gamma)
            diff_prob = fam_score - old_famscore
            self.diff_cache.add(diff, diff_prob)
        self.cooper_likelihood += diff_prob

        # old_famscore = self.calc_fam_score(nodes[to], self.data)
        # self.dag.add_edge(nodes[fr], nodes[to])
        # self.update_likelihood(old_parent_ids, old_famscore , nodes[to])

    def del_edge(self, edge):
        nodes = self.nodes
        fr = edge[0]
        to = edge[1]

        if self.data is None:
            self.dag.remove_edge(nodes[fr], nodes[to])
            return

        tmp = [pre.id for pre in self.dag.predecessors(nodes[to])]

        tmp.sort()
        old_parent_ids = tuple(tmp)

        tmp.remove(fr)
        new_parent_ids = tuple(tmp)
        diff_prob = 0
        diff = (to, old_parent_ids, new_parent_ids)
        rev_diff = (to, new_parent_ids, old_parent_ids)

        if self.diff_cache.contains(diff):
            diff_prob = self.diff_cache.get(diff)
            self.dag.remove_edge(nodes[fr], nodes[to])
        elif self.diff_cache.contains(rev_diff):
            diff_prob = -self.diff_cache.get(rev_diff)
            self.dag.remove_edge(nodes[fr], nodes[to])
        else:
            old_famscore = self.calc_fam_score(nodes[to],
                                               self.data,
                                               self.prior,
                                               self.gamma)
            self.dag.remove_edge(nodes[fr], nodes[to])
            fam_score = self.calc_fam_score(nodes[to],
                                            self.data, self.prior, self.gamma)
            diff_prob = fam_score - old_famscore
            self.diff_cache.add(diff, diff_prob)
        self.cooper_likelihood += diff_prob

#        old_famscore = self.calc_fam_score(nodes[to], self.data)
#        self.dag.remove_edge(nodes[fr],nodes[to])
#        self.update_likelihood(old_parent_ids, old_famscore , nodes[to])

    def update_likelihood(self, old_parent_ids, old_famscore, node):
        """ Updates the likelihood and adds it to the cache.
            Need to know: node, prev parents, likelihood for prev parents
        Args:
             old_node: is the previous node with parents and fam_score
             new_node: is the new node with parents and fam_score
        """
        tmp = [pre.id for pre in self.dag.predecessors(node)]
        tmp.sort()
        new_parent_ids = tuple(tmp)

        id = node.id
        diff_prob = 0
        diff = (id, old_parent_ids, new_parent_ids)
        rev_diff = (id, new_parent_ids, old_parent_ids)

        if self.diff_cache.contains(diff):
            diff_prob = self.diff_cache.get(diff)
        elif self.diff_cache.contains(rev_diff):
            diff_prob = -self.diff_cache.get(rev_diff)
        else:
            new_famscore = self.calc_fam_score(node, self.data,
                                               self.prior, self.gamma)
            diff_prob = new_famscore - old_famscore
            self.diff_cache.add(diff, diff_prob)

        self.cooper_likelihood += diff_prob

    def get_gamma(self, node, prior, g=1.0):
        if prior == "BDeu":
            r = [p.range for p in self.dag.predecessors(node)]
            parent_ranges = np.prod(np.array(r))
            return g / (node.range * parent_ranges)
        elif prior == "K2":
            return g
        return None

    def cooper_herzkovitz(self, data, prior, gamma):
        """
        prior: "BDeu" or "K2"
        gamma: pthe parameter for the prior.
        """
        if data is None:
            return 0
        ln_score = 0
        for node in self.dag.nodes():
            ln_s = self.calc_fam_score(node, data, prior, float(gamma))
            ln_score += ln_s
        return ln_score

    def calc_fam_score(self, node, data, prior, gamma):
        score = 0
        # First count the occurrences
        self.count_node(data, node)

        g = self.get_gamma(node, prior, gamma)
        for parent_conf, node_conf_list in node.counts.items():
            score += ss.gammaln(g * node.range)
            score -= ss.gammaln(g * node.range + node_conf_list.sum())  # TODO
            for i, node_conf in enumerate(node_conf_list):
                score += ss.gammaln(g + node_conf) - ss.gammaln(g)

        node.fam_score = score
        return score

    def count_node(self, data, node):
        """ Gets the family/node configurations in data of a node.
            Saves the date in node.counts
            Args:
                 node:
                 data:
        """
        node.counts = {}
        for j, row in enumerate(data):
            if self.dag.predecessors(node) == []:
                # If no parents
                if () not in node.counts:
                    node.counts[()] = np.zeros(node.range, dtype=int)
                node.counts[()][row[node.id]] += 1

            if not self.dag.predecessors(node) == []:
                pred_index = [p.id for p in self.dag.predecessors(node)]
                pred_index.sort()
                if not tuple(row[pred_index]) in node.counts:
                    node.counts[tuple(row[pred_index])] = np.zeros(node.range,
                                                                   dtype=int)
                node.counts[tuple(row[pred_index])][row[node.id]] += 1

    def get_pygraphviz(self):
        return nx.to_agraph(self.dag)

    def count_parent_conf(self, node, parent_conf):
        if parent_conf not in node.counts:
            return 0
        return node.counts[parent_conf].sum()

    def count_parent_node_conf(self, node, node_conf, parent_conf):
        if parent_conf not in node.counts:
            return 0
        return node.counts[parent_conf][node_conf]

    def get_tuple(self):
        """ This is the tuple representation of the graph
        """
        tmp = self.get_numpy_matrix()
        tmp = np.array(tmp)
        tmp = tmp.flatten()
        tmp = tmp.tolist()
        tmp = tuple(tmp)
        return tmp

    def __hash__(self):
        return hash(self.get_tuple())

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def __ne__(self, other):
        return not self == other

    def cpds_tofile(self, filename):
        """ write to file """
        res = {"cpds": [node.cpd.tolist() for node in self.nodes]}
        with open(filename, 'w') as outfile:
            json.dump(res, outfile, indent=4, separators=(',', ': '))

    def get_numpy_matrix(self):
        n = self.dag.order()
        mat = np.zeros(n*n, dtype=int)
        nodes = self.nodes
        mat.shape = (n, n)

        for node in nodes:
            for succ in self.dag.successors(node):
                mat[node.id][succ.id] = 1

        return np.matrix(mat)

    def plot_heat_map(self, path, filename, title):
        matrix = self.get_numpy_matrix()
        ploting.plot_heat_map(matrix, path, filename, title)

    def plot(self, path, filename):
        p = self.get_pygraphviz()

        if not p.nodes()[0].attr["color"] is None:
            same_colors = {}
            for n in p.nodes():
                color = n.attr["color"]
                if color not in same_colors:
                    same_colors[color] = []
                same_colors[color].append(n)

            for key, value in same_colors.iteritems():
                p.add_subgraph(value, rank="same")

        p.node_attr['shape'] = 'circle'
        p.layout("dot")
        p.draw(path+filename+".eps")
