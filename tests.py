import unittest
from test_crp import *
from test_restgraph import *
from test_lib import *
from test_mcmc import *
from test_bn import *

suits = []
suits.append(unittest.TestLoader().loadTestsFromTestCase(TestCRP))
suits.append(unittest.TestLoader().loadTestsFromTestCase(TestRestGraph))
#suits.append(unittest.TestLoader().loadTestsFromTestCase(TestLib))
suits.append(unittest.TestLoader().loadTestsFromTestCase(TestMCMC))
suits.append(unittest.TestLoader().loadTestsFromTestCase(TestBN))
suits.append(unittest.TestLoader().loadTestsFromTestCase(TestLikelihood))


for suite in suits:
	unittest.TextTestRunner(verbosity=2).run(suite)



