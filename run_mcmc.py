import json
import sys
import logging
import numpy as np
import os
import mcmc

# usage example
# python run_mcmc.py CONF_FILE.json water1000 data_path save_path log_level

# Properties
json_data = open(sys.argv[1]).read()
conf = json.loads(json_data)
c = sys.argv[2]
path = sys.argv[3]+"/"+c
data_path = sys.argv[4]
log_level = int(sys.argv[5])

# Read conf
print "Running conf: "+str(c)

random_seed = conf[c]["random_seed"]

mode = conf[c]["mode"]
data_file = conf[c]["data_file"]

iterations = conf[c]["iterations"]
chain_pool_size = conf[c]["chain_pool_size"]
b1 = conf[c]["beta_1"]
b2 = conf[c]["beta_2"]
alpha = conf[c]["alpha"]
alpha_mcmc = conf[c]["alpha_mcmc"]
support = conf[c]["support"]
prior = conf[c]["parameter_prior"]
gamma = conf[c]["gamma"]

if not os.path.exists(path):
    os.makedirs(path)

logging.basicConfig(filename=path+'/mcmc.log', level=log_level)

data = np.loadtxt(data_path+data_file, skiprows=0, dtype=int)
n = len(data[0])

if len(support) == 1:
    support = support*n

# McMC
(zs, cooper, prob, dags, cputimes) = mcmc.pool(data, chain_pool_size,
                                               iterations, alpha, b1, b2,
                                               alpha_mcmc, mode, support,
                                               prior, gamma, path)
