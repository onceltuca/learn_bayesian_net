import networkx as nx
import numpy as np

def get_mcmc_results(pool,path):
    """ Read """
    dags = []
    zs = []
    prob = []
    cooper = []
    cputimes = []
    for i in pool: 
        tmp = np.loadtxt(path+'/mcmc_dags_'+str(i)+'.txt', skiprows=0,dtype=int)
        ds = [ nx.DiGraph(np.matrix(d).reshape(np.sqrt(len(d)),np.sqrt(len(d)))) for d in tmp]
        dags.append(ds)

        tmp = np.loadtxt(path+'/mcmc_zs_'+str(i)+'.txt', skiprows=0,dtype=int)
        tmp = tmp.tolist()
        ztmp = [tuple(z) for z in tmp]
        zs.append(ztmp)
        tmp = np.loadtxt(path+'/mcmc_cooper_'+str(i)+'.txt', skiprows=0,dtype=float)
        cooper.append(tmp.tolist())
        tmp = np.loadtxt(path+'/mcmc_prob_'+str(i)+'.txt', skiprows=0,dtype=float)          
        prob.append(tmp.tolist())
        tmp = np.loadtxt(path+'/mcmc_cputimes_'+str(i)+'.txt', skiprows=0,dtype=float)     
        cputimes.append(tmp.tolist())      
    return (zs,cooper,prob,dags,cputimes)

def merge_chains(zs,cooper,prob,dags,cputimes):
    """ Analyse """
    all_probs = np.concatenate(prob)  
    all_cooper = np.concatenate(cooper)    
    #sort_by_prob_index = np.argsort(all_probs)
    #all_probs_sorted = np.array([all_probs[p] for p in sort_by_prob_index])
    all_dags = reduce(list.__add__, dags, [])
    #all_dags_sorted = [all_dags[d] for d in sort_by_prob_index]
    all_zs = np.concatenate(zs)
    #all_cpu_times  = [a.tolist() for a in cputimes]
    all_cpu_times  = cputimes
    #all_zs_sorted = [all_zs[d] for z in sort_by_prob_index]
    return (all_probs,all_cooper,all_dags,all_zs,all_cpu_times)
