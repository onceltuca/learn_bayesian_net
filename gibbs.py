from dagpost import *
import numpy as np
import scipy
import scipy.stats
import variable as bnvar
import networkx as networkx
import bayesian_network as bn
from random import randint
import random 
import scipy.special as ss
import scipy.misc as sc
import matplotlib.pyplot as plt
import ploting
import os
import json
import sys
import logging
import pprint as pp

## Properties
json_data=open(sys.argv[1]).read()
conf = json.loads(json_data)
c = sys.argv[2]

data_path = sys.argv[4]
log_level = int(sys.argv[5])

# Read conf
print "Running conf: "+str(c)

random_seed = conf[c]["random_seed"]
random.seed(random_seed)# 1
np.random.seed(random_seed)# 1

mode = conf[c]["mode"]
data_file = conf[c]["data_file"]
data_sets = conf[c]["data_sets"]
T = conf[c]["iterations"]
chain_pool_size = conf[c]["chain_pool_size"]
beta_1 = conf[c]["beta_1"]
print beta_1[0][1]
beta_2 = conf[c]["beta_2"]
alpha = conf[c]["alpha"]

support = conf[c]["support"]
prior = conf[c]["parameter_prior"]
dag_prior = conf[c]["dag_prior"]
gamma = conf[c]["gamma"]

path = sys.argv[3]+"/"+c+"/"#+"/alpha_mcmc_"+str(alpha_mcmc)+"_alpha_"+str(alpha)+"_beta1_"+str(beta_1)+"_beta2_"+str(beta_2)+"/"

if not os.path.exists(path):
    os.makedirs(path)

logging.basicConfig(filename=path+'/mcmc.log',level=log_level)

data = np.loadtxt(data_path+data_file, skiprows=0,dtype=int)
n = len(data[0])

if len(support) == 1: support = support*n

# Stochastic search
# Start with empty graph
for d in range(data_sets):
    for j in range(chain_pool_size):
        nodes = [ bnvar.Variable(i, support[i]) for i in range(n) ]
        network = bn.BayesianNetwork()
        network.set_nodes(nodes)
        network.set_data(data, prior, gamma)
        z = np.zeros(n, dtype=int)
        nets = []
        zs = []
        dags = []
        likelihoods = []
        priors = []
        for r in range(1,T):
            fr = range(n)
            random.shuffle(fr)
            to = range(n)
            random.shuffle(to)
            print "dataset: "+str(d) +"/"+str(data_sets)+", chain: "+str(j) +", sim: " + str(r) 
            #print network
            print "z: "+str(z_str(z))
            for x in fr:
                for y in to:
                    #print "dataset: "+str(d) +"/"+str(data_sets)+", chain: "+str(j) +", sim: " + str(r) 
                    #assert(get_bad_edges(network, z, range(n)) == [])   
                    #print network
                    print "layers: "+str(z_str(z))
                    print "edge: "+ str((x,y))
                                        
                    if not is_addable(z, network, (x,y)):
                        print "P("+str((x,y))+"|G\\"+str((x,y))+") = 0.0"
                        continue

                    existed = network.has_edge((x,y))

                    prior_plus = 0.0
                    prior_minus = 0.0
                    ll_plus = 0.0
                    ll_minus = 0.0
                    if existed:
                        #print str((x,y)) + " exists"
                        if dag_prior == "hoppe":
                            prior_plus = dag_log_density(network, z, alpha, beta_1, beta_2)

                        ll_plus = network.cooper_likelihood            
                        #print "removing for calc prob"
                        remove_edge(z, network, (x,y))

                        if dag_prior == "hoppe":
                            prior_minus = dag_log_density(network, z, alpha, beta_1, beta_2)

                        ll_minus = network.cooper_likelihood
                    else:
                        #print str((x,y)) + " does not exist"
                        if dag_prior == "hoppe":
                            prior_minus = dag_log_density(network, z, alpha, beta_1, beta_2)

                        ll_minus = network.cooper_likelihood
                        #print "adding for calc prob"
                        add_edge(z, network, (x,y))

                        if dag_prior == "hoppe":
                            prior_plus = dag_log_density(network, z, alpha, beta_1, beta_2)

                        ll_plus = network.cooper_likelihood
                    log_post_plus = prior_plus + ll_plus
                    log_post_minus = prior_minus + ll_minus
                    edge_prob = 1.0 / (1.0 + np.exp( log_post_minus - log_post_plus ) ) 
                    print "P("+str((x,y))+"|G\\"+str((x,y))+") = " + str(edge_prob)

                    accept = np.random.binomial(1, edge_prob )
                    if accept:
                        if existed:
                            #print "Keeping "  
                            add_edge(z, network, (x,y))    # else is ok
                        else:
                            pass
                            #print "Adding " 
                    if not accept:                        
                        if not existed:
                            #print "Not adding " 
                            remove_edge(z, network, (x,y)) # else is 
                        else:
                            pass
                            #print "Removing " 

            zs += [list(z)]
            dags += [np.array(network.get_numpy_matrix().flatten())[0]]
            likelihoods += [network.cooper_likelihood]
            log_prior = 0.0
            if dag_prior == "hoppe":
                log_prior = dag_log_density(network, z, alpha, beta_1, beta_2)
            priors += [log_prior]
            np.savetxt(path+'/dataset_'+str(d)+'_gibbs_dags_chain_'+str(j)+'.txt',dags, fmt='%.1i')
            np.savetxt(path+'/dataset_'+str(d)+'_gibbs_zs_chain_'+str(j)+'.txt',zs, fmt='%.1i')   
            np.savetxt(path+'/dataset_'+str(d)+'_gibbs_cooper_chain_'+str(j)+'.txt',likelihoods, fmt='%10.15f')
            np.savetxt(path+'/dataset_'+str(d)+'_gibbs_priors_chain_'+str(j)+'.txt',priors, fmt='%10.15f')

with open(str(path)+'/settings.json', 'w') as outfile:
  json.dump(conf[c], outfile, indent=4, separators=(',', ': '))
