import numpy as np
import scipy
import scipy.stats
from scipy.stats import bernoulli
from pygraphviz import *
import pygraphviz
import networkx as nx
import scipy.special as ss
import operator
import bayesian_network as bn
import copy
from collections import deque
from table import *
from person import *
import scipy.misc as sc

#np.random.seed(1)
class Restaurant :
    """ A chinese restaurant process object.
        It can generate a random partition accoring to the CRP.
        More it can remove a person from its table this is used in the
        proposal for the MCMC.
    """
    def __init__(self, concentration, no_persons, id ) :
        # starting new
        self.z = []
        self.m = []
        self.concentration = concentration        
        self.no_persons = no_persons
        self.tables = [] # Only here are the objects
        self.persons = [ Person(i) for i in range(no_persons)] # Only here are the objects
        self.id = id        

    def expectation(self):
        e = 0
        a = self.concentration
        for i in range(self.no_persons):
            e += np.float(a)/np.float(a+i)
        return e

    def copy(self):
        ret = Restaurant(self.concentration,
                         self.no_persons, 
                         self.id) 
        ret.tables = copy.deepcopy(self.tables)
        ret.persons = copy.deepcopy(self.persons)                

         # Update the tables for the persons
        for p in ret.persons :
            i = p.table.id
            p.table = ret.tables[i] 
        return ret

    def get_partition_matrix(self):
        n = self.no_persons
        m = np.zeros((n,n),dtype=int)
        for p1 in self.persons:
            for p2 in self.persons:        
                if p1.table.id == p2.table.id:
                    m[p1.id][p2.id] = 1
        return m
    
    def get_partition_matrix_as_tuple(self):
        return tuple(self.get_partition_matrix().flatten())
    
    def get_partition(self):
        partition = set()
        for table in self.tables:
            partition.add(tuple(table.persons))
        return partition

    def __hash__(self):
        #return hash(self.get_z())
        return hash(self.get_partition_matrix_as_tuple())
    
    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def __ne__(self, other):
        return not self == other

    def get_z(self):
        z = []
        i = 0
        for p in self.persons:
            assert(p.id==i)            
            z.append(p.table.order)
            #z.append(p.table.id)
            i = i+1
        return tuple(z)
    
    def assign_person(self, p, n) :
        """ Assigns a person to a table.
            Args:
            n: the number of persons currently in the restaurant (except for this person)
            p: person to assign
        """
        #print "-------- In assign_person("+str(p),str(n)+") --------"        
        rand = np.random.random() # [0,1]
        p_total = 0
        existing_table = False
        ordered_tables = sorted(self.tables, key=lambda t: t.order)        

        for index, table in enumerate(ordered_tables):
            prob = np.float(len(table.persons)) / np.float(n + self.concentration)
            p_total += prob
            if rand < p_total:
                # print "Insert person "+str(p.id)+" at table "+str(index)
                p.set_table(self.tables[index])
                self.tables[index].add_person(p)
                existing_table = True
                break
        if not existing_table:
            t = Table(len(self.tables),p)
            self.tables.append(t)
            
    def assign_persons(self,order = "random"):
        """ Assigns a permutation of the persons the the tables.

            This makes it possible for any person to be at the first table.
            This is only used as a starting distribution in th MH.        

            Args: 
                order: If random, the persons sits at random. 
                       Else, person 0 is always at first table.
        """
        porder = range(self.no_persons)

        if order == "random":
            porder = np.random.permutation(porder)

        t = Table(0, self.persons[ porder[0] ])
        self.tables.append(t)        
        for n in range(1,self.no_persons):
            self.assign_person(self.persons[porder[n]],n)

    def __str__(self):
        ordered_tables = [0]*len(self.tables)
        for i,table in enumerate(self.tables):
            ordered_tables[table.order] = table

        s = "Restaurant("+str(id(self))+")"        
        for t in ordered_tables:
            s += "\n"+str(t)
        return s
      
    def set_state(self,assignments):
        """ Set partition

        This makes it possible to set a partition to the process,
        it is used when we want to reassign person  e.g. in th MH 

        Args:
        assignments: list of person ids [[2,3,6],[0,1]] means 2,3,6 sits at table 0, 0,1 sits at table 1      
        """
        tables = []
        for i,table_ids in enumerate(assignments):
            for j,pid in enumerate(table_ids):
                if j == 0: 
                    tables.append(Table(i,self.persons[pid]))
                else: 
                    tables[i].add_person(self.persons[pid])
        self.tables = tables

    def set_empty_table(self, table):
        self.empty_table = table

    def leave(self,p):
        """ Remove a person from the restaurant.        
            Args:
                p: person object
        """
        t = p.table
        t.remove_person(p)
        if t.is_empty() :
            #print "Old table empty - Recalculate orders and id for the tables"
            self.decrease_table_orders_from(t.order)
            self.decrease_table_ids_from(t.id)
            self.tables.remove(t)
        # No need to remove the person
        # self.persons.remove(p)
         
    def reassign_person(self,p_id):
        """ A random person leaves and enters the restaurant again
        Returns:
        A tuple {"person":p,"old_table":old_table}
        """                        
        p = self.persons[p_id]        
        #print "Reassign person: "+str(p)
        #print "Old table: "+str(p.table)
        old_table = copy.deepcopy(p.table) 
        self.leave(p)
        self.assign_person(p,len(self.persons))
        new_table = p.table
        #print "New table: "+str(p.table)
        return old_table
              
    def decrease_table_ids_from(self,tid):
        for table in self.tables :
            if table.id >= tid:
                table.id = table.id - 1

    def decrease_table_orders_from(self,order):
        for table in self.tables :
            if table.order > order:
                table.order = table.order - 1 

    def prob_order_given_setting(self):
        """ Calculates the probability ord the order given the person assignment.
        """
        return -np.log(sc.factorial(len(self.tables)) ) # TODO: change to sum
        
    def prob_person_assignment_marginal_perm(self):
        """ Calculates the marginal probability of the state integrated over
            all the possible permutations over the persons.
        """
        K = len(self.tables)
        N = self.no_persons
        p = self.prob_person_assignment_standard_perm()

        for k in range(K):
            p += np.log(sc.factorial(len(self.tables[k].persons)))         
        p -= np.log(sc.factorial(N)) # prob of perosn assignment
        return p
        
    def prob_person_assignment_standard_perm(self):
        """ Calculates the probability of the state for one permutations of the persons.
        """
        K = len(self.tables)
        N = self.no_persons
        a = self.concentration
        p = 0
        p += K*np.log(a)
        p += ss.gammaln(a)
        p -= ss.gammaln(N+a)
        for k in range(K):
            p += np.log(sc.factorial(len(self.tables[k].persons) - 1)) #TODO: minus ?    
        return p  

    def reorder_table(self, t, new_order):
        """ Reorder table
            Args:
                t: The table to reorder.
                new_order: New order.
        """

        old_order = t.order
        # Move up table
        if old_order < new_order:
            for table in self.tables:
                if table.order > old_order and table.order <= new_order:
                    table.order -= 1

        # Move down
        if old_order > new_order:
            for table in self.tables:
                if table.order >= new_order and table.order < old_order :
                    table.order += 1
        t.order = new_order
     
    def shuffle_tables(self): 
        """ Randomize the order of the tables.
            
            The order is actually not part of the process and it might be ok to
            use the ids instead. 

            TODO: This might not even be in this class.
        """      
        orders = np.random.permutation(range(len(self.tables)))
        self.tables = list(self.tables)
        j=0
        for i in orders:            
            self.tables[j].order = i
            j += 1

    def swap_tables(self, t1, t2):
        t1_order = t1.order
        t1 = t2.order
        t2 = t1_order
       
    def get_number_of_persons_of_higher_order(self,p):
        total = 0
        for t in self.tables:
            if t.order > p.table.order:
                total += len(t.persons)
        return total

    def print_persons(self):
        debug_mode = "live"
        debug_mode = "debug"

        if debug_mode == "debug":
            print self.get_settings()
            for i,table in enumerate(self.tables):
                print "Persons with order "+str(table.order)+ "(id:"+str(table.id)+ "): " 
                for j,p in enumerate(table.persons):
                    print self.persons[p].id
                print ""
    
    def print_table_order(self):
        for i,table in enumerate(self.tables):
            print "Table "+str(table.id)+" has order "+str(table.order)+ " and nu vector "+str(self.nu[table.order])


    def get_settings(self):
        return "Id: "+str(self.id)+" Nodes: "+str(self.no_persons)+", Alpha: "+str(self.concentration)+", Beta: "+str(self.beta_1)+", "+str(self.beta_2)

    def same_table_matrix(self):
        n = self.no_persons
        classes = np.zeros(n*n).reshape(n,n)

        for i,pi in enumerate(self.persons):
            for j,pj in enumerate(self.persons):
                if  pi.table == pj.table:
                    classes[i][j] = 1
                    classes[j][i] = 1
        return classes
# TODO: Color nodes by table! important for the analysis!

