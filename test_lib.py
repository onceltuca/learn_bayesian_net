import unittest
import networkx as nx
from help_functions import *
import math


class TestLib(unittest.TestCase):
    def test_orders(self):
        partial_orders = [(2,1),(1,0)]        
        n = 3
        o = orders_rel(partial_orders, n)
        self.assertTrue(o == 1)

    def test_get_static_subgraphs(self):
        dag = nx.DiGraph()
        n = 5
        dag.add_edges_from([(1,3),(3,4),(2,4)])
        dag.add_nodes_from(range(n))
        get_static_sets(dag)

    def test_top_orders(self):
        n = 4
        o = count_partial([(2,1),(1,0),(2,3)], n)
        self.assertTrue(o == 3)

        n = 4
        o = count_partial([], n)
        self.assertTrue(o == math.factorial(n))

        partial_orders = [(1,3),(3,4),(2,4)]
        n = 5
        dag = nx.DiGraph()
        dag.add_edges_from([(1,3),(3,4),(2,4)])
        dag.add_nodes_from(range(n))        
        o = count_partial(partial_orders, 5)
        self.assertTrue(o == 15)

        n = 17
        partial_orders = [(2,3),(3,13),(1,2),(1,10),(1,4),(1,11),(8,10),(8,15),(4,14),(4,15),(4,6),(10,14),(14,15),(11,14),(7,9),(5,16),(6,9),(6,15),(9,10)]
        o = count_partial(partial_orders, n)

        self.assertTrue(o == 891928800)

if __name__ == '__main__':
    unittest.main()
    

