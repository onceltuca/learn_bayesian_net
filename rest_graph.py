from help_functions import *
import numpy as np
from pygraphviz import *
import pygraphviz
import networkx as nx
import scipy.special as ss
import operator
import bayesian_network as bn
import variable as bnvar
import copy
from collections import deque
from table import *
from person import *
import scipy.misc as sc
import pprint as pp
import ploting

class RestGraph :

    def __init__(self, rest, beta_1, beta_2, mode):
        """ Constructor
            Args:
                rest: Restaurant object
                beta_1: hyper parameter for the edge probability
                beta_2: hyper parameter for the edge probability
        """
        # New version
        # self.table_order_rho = [...]
        # self.person_sigma = range(rest.no_pers())#[2,0,1]  means person 0 has order 2 etc
        # Should not be needed since it is integrated out.
        # but then person 1 always sits at table 1.
        # So it should be updated.
        # The order of the persons has a meaning in the DAG.
        # however we would like to be able to test different orders
        # in the algorithm.
        #
        # Seems like formally we test many different permutations in 
        # the algorithm since every node can be any where.
        # And the order do have significance.
        self.mode = mode
        self.rest = rest
        self.beta_1 = beta_1
        self.beta_2 = beta_2
        self.network = None
        n = rest.no_persons
        self.nu = np.zeros(n*n).reshape(n,n)
    def __str__(self):
        s = "Restgraph, beta1="+str(self.beta_1)+"beta2="+str(self.beta_2)+"\n"
        s += self.prob_string()+"\n"
        s += str(self.rest)+"\n"
        s += str(self.network)+"\n"        
        #s += "nu matrix: \n"
        #s += str(self.nu)+"\n"
        return s

    def copy(self):
        r = self.rest.copy()
        ret = RestGraph(r,self.beta_1, self.beta_2, self.mode)
        ret.network = self.network.copy()
        ret.nu = self.nu
        return ret

    def set_network(self, net):
        """ Set a specific network
        """
        self.network = net
        
    def create_network(self, data, support, prior, gamma ): 
        """Creates a BN
        """

        nodes = [ bnvar.Variable(i,support[i]) for i in range(self.rest.no_persons) ]

        self.network = bn.BayesianNetwork()
        self.network.set_nodes(nodes)
        #self.network.color_nodes(self.rest.get_z())
        #self.network.set_class(self.rest.get_z())
        self.network.set_data(data, prior, gamma)

        # This is for the conditional graph probability
        #self.edges_between_tables = np.zeros(self.no_persons*self.no_persons,dtype=int).reshape(self.no_persons,self.no_persons)
        for i in range(self.rest.no_persons) :
            self.create_edges_from_person(i,len(self.rest.tables))

    def get_table_graph(self):
        partial_orders = []        
        # Build help graph
        for t in self.rest.tables:
            for i in t.persons:
                # look at children
                nodei = self.network.get_nodes()[i]
                for child in self.network.dag.successors(nodei):
                    e = (t.order, self.rest.persons[child.id].table.order)
                    #print e
                    if e not in partial_orders and not t.order == self.rest.persons[child.id].table.order:
                        partial_orders.append(e)
 
        dag = nx.DiGraph()
        dag.add_edges_from(partial_orders)
        dag.add_nodes_from(range(len(self.rest.tables)))
        return dag

    def ln_g(self):
        dag = self.get_table_graph()
        res = count(dag)        
        return np.log(res)

    def get_bad_edges(self,persons):
        edges = []
        for p in persons:
            node = self.network.get_nodes()[p.id]
            for child in self.network.dag.successors(node) :
                ch = self.rest.persons[child.id]
                child_table = ch.table
                if child_table.order <= p.table.order and (p.id,ch.id) not in edges:                    
                    edges += [(p.id,ch.id)]

            for parent in self.network.dag.predecessors(node) :
                pa = self.rest.persons[parent.id]
                parent_table = pa.table
                if parent_table.order >= p.table.order and (pa.id,p.id) not in edges:
                    edges += [(pa.id,p.id)]        
        return edges

    def create_edges_from_person(self, i, max_order, edge_prob = None):
        """ Creates all possible edges from a person with probability edge_prob.
            Args:
            i: is the id of the person
        """
        from_t = self.rest.persons[i].table
        for j in range(self.rest.no_persons):
            to_t = self.rest.persons[j].table
            if from_t.order < to_t.order and to_t.order <= max_order :
                if edge_prob is None:
                    edge_prob = self.nu[from_t.order, to_t.order ] 
                e = np.random.binomial(1,edge_prob)
                if e:
                    self.network.add_edges([(i,j)])
                    #self.edges_between_tables[self.persons[i].table.id][self.persons[j].table.id] += 1
    def create_edges_to_person(self, i, min_order, edge_prob = None):
        """Creates edges to a node according to the nu matrix
        Args:
        i: The id of the person
        """
        to_t = self.rest.persons[i].table
        for j in range(self.rest.no_persons) :
            from_t = self.rest.persons[j].table
            if to_t.order > from_t.order and from_t.order >= min_order :
                #print "about to add "+str((j,i))            
                if edge_prob is None:
                    edge_prob = self.nu[from_t.order, to_t.order ]
                e = np.random.binomial(1,edge_prob) # TODO: set to 0.5.
                if e:
                    #print "Adding edge: "+str((j,i))
                    self.network.add_edges([(j,i)])
                    #self.edges_between_tables[self.persons[j].table.id][self.persons[i].table.id] += 1

    def get_edges_between_tables(self):
        K =  len(self.rest.tables)
        edges_between_tables = np.zeros(K*K,dtype=int).reshape(K,K)
        for n1 in self.network.get_nodes() :
            p1 = self.rest.persons[n1.id]
            for child in self.network.dag.successors(n1) : # Choosing children here
                p2 = self.rest.persons[child.id]
                edges_between_tables[p1.table.id][p2.table.id] += 1
        return edges_between_tables

    def get_prob_graph_given_o_z(self,mode):
        """    
        This is depending on how the graph is generated. (See set_edge_prob(mode)).

        Returns: the log probability of the graph given the order and the assignments
        """
        log_p = 0
        K = len(self.rest.tables)
        N = len(self.rest.persons)
        if mode == "single":
            existing_edges = len(self.network.dag.edges())            
            possible_edges = 0

            for i in range(K):
                for j in range(i,K):
                    possible_edges += len(self.rest.tables[i].persons) * len(self.rest.tables[j].persons)                    
            missing_edges = possible_edges - existing_edges
            log_p = ss.betaln(self.beta_1 + existing_edges, self.beta_2 + possible_edges ) 
            log_p -= ss.betaln(self.beta_1, self.beta_2)

        if mode == "all":
            for i in range(N):
                for j in range(N):
                    if self.rest.persons[i].table.order < self.rest.persons[j].table.order:
                        e = 0
                        if self.network.has_edge((i,j)): e = 1
                        log_p += ss.betaln(self.beta_1 + e, self.beta_2 + (1-e) ) 
                        log_p -= ss.betaln(self.beta_1,self.beta_2)

        if mode == "group":                        
            n1 = self.get_edges_between_tables()         
            for a in self.rest.tables:
                for b in self.rest.tables:
                    if a.order < b.order:
                        poss =  len(a.persons) * len(b.persons) 
                        n2 = poss - n1[a.id,b.id] #missing_edges_between_tables
                        log_p += ss.betaln(self.beta_1 + n1[a.id,b.id], self.beta_2 + n2 ) 
                        log_p -= ss.betaln(self.beta_1,self.beta_2)
        return log_p

    def set_edge_prob(self,mode) :
        """ Creates a matrix of edge probababilities. 
            Thera are different versions here, depending on how we choose the prior.

            1. (all)    Each edge has its own edge probability (but eq distributed),
                        we restrict i to beta1 and beta2.
            2. (single) There is only one edge probability.
            3. (group)  Each pair of classes has their own edge probability.
        """
        K = len(self.rest.tables)
        N = len(self.rest.persons)
        if mode == "single":
            nu = np.random.beta(self.beta_1,self.beta_2)
            for i in range(N) :
                for j in range(i+1, N) :
                    self.nu[i][j] = nu

        if mode == "all":
            for i in range(N) :
                for j in range(i+1,N) :
                    self.nu[i][j] = np.random.beta(self.beta_1,self.beta_2)

        if mode == "group":
            for i in range(K) :
                for j in range(i+1,K) :
                    self.nu[i][j] = np.random.beta(self.beta_1,self.beta_2)

        if mode == "indepcause":
            nu = np.random.beta(self.beta_1,self.beta_2)
            for i in range(K) :
                self.nu[i][i+1] = nu

    def prob(self):
        p = 0
        # P(x|D,z)
        p += self.network.cooper_likelihood
        # P(z)
        p += self.rest.prob_person_assignment_marginal_perm()
        # P(D|z): 
        p += self.ln_g()
        #p += np.log(sc.factorial(len(self.rest.tables)))
        
        p += self.get_prob_graph_given_o_z(self.mode)
        p += self.rest.prob_order_given_setting()
        return p
    
    def prob_string(self):
        p = 0
        ret = ""
        ret += "Cooper-Herzkovitz P(x|d)= "+str(self.network.cooper_likelihood) +" ("+str(np.exp(self.network.cooper_likelihood))+")"
        ret += "\n"
        p += self.network.cooper_likelihood
#        ret += "Prob order = "+str(self.rest.prob_order_given_setting()) +" ("+str(np.exp(self.rest.prob_order_given_setting()))+")"
#        ret += "\n"
        p += self.rest.prob_order_given_setting()
        ret += "ln P(z) = " + str(self.rest.prob_person_assignment_marginal_perm())+" ("+str(np.exp(self.rest.prob_person_assignment_marginal_perm()))+")"
        ret += "\n"
        p += self.rest.prob_person_assignment_marginal_perm()
        ret += "ln P(d|z,rho) = "+str(self.get_prob_graph_given_o_z(self.mode)) +" ("+str(np.exp(self.get_prob_graph_given_o_z(self.mode)))+")"
        p += self.get_prob_graph_given_o_z(self.mode)
        ret += "\n"
        tops = self.ln_g()        
        ret += "ln g(d,z) = "+str(tops) + " ("+str(np.exp(tops))+")"
        p += tops
        ret += "\n"
        ret += "Tot: "+str(p)
        return ret

    def plot_heat_map(self, path, filename, title):

        adj_matrix = self.network.get_numpy_matrix()
        
        ploting.plot_heat_map(adj_matrix, path, filename+"_graph", title)
        partition_matrix = self.rest.get_partition_matrix()
        ploting.plot_heat_map(partition_matrix, path, filename+"_partition", title)

        self.network.color_nodes(self.rest.get_z())

        agraph = self.network.get_pygraphviz()

        #for n in agraph.nodes():
       #     print n.attr
        
        agraph.node_attr['shape']='circle'
        agraph.layout("dot")
        agraph.draw(path+filename+"_dag.eps")

    def plot(self, path, filename):
        p = self.network.get_pygraphviz()
        p.node_attr['shape']='circle'
        p.layout("dot")
        p.draw(path+filename+".eps")
        
    def get_tuple(self):
        return (self.rest.get_partition_matrix_as_tuple(), self.network.get_tuple())

    def to_file():
        self.rest.z

        np.savetxt(path+'/mcmc_dags_'+str(i)+'.txt',dags, fmt='%.1i')
        np.savetxt(path+'/mcmc_zs_'+str(i)+'.txt',zs, fmt='%.1i')   

    def __hash__(self):
        return hash(self.get_tuple())

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def __ne__(self, other):
        return not self == other
