import numpy as np
import scipy
import networkx as nx
from random import randint
import scipy.special as ss
import scipy.misc as sc


def insert_at_existing(z, n, t):
    """
    1) Check if any move at all.
    2) Remove from layer. fix i it was the only one int the table.
    3) Add to layer. fix if z[n] < t
    3) Check if the old layer is empty. Fix in thet case.
    """

    if t > max(z):
        raise Exception
    # 1) no move
    if z[n] == t:
        return
    # 2)
    cur_layer = z[n]
    new_layer = t
    m = np.zeros(len(z))
    for i, j in enumerate(z):
        m[j] += 1

    if m[cur_layer] == 1:
        # fix syntax
        if cur_layer < new_layer:
            new_layer -= 1

        # 3) move from single table
        for i in range(len(z)):
            if z[i] > cur_layer:
                z[i] -= 1
    z[n] = new_layer

    return


def insert_at_new(z, n, t):
    # Fix if only in table
    m = np.zeros(len(z))
    for i, j in enumerate(z):
        m[j] += 1

    # Flytta till samma och ensam
    if z[n] == t and m[z[n]] == 1:
        return

    # Check if possible
    if m[z[n]] == 1 and t > max(z):
        raise Exception
    if m[z[n]] > 1 and t == max(z) + 1:
        z[n] = t
        return

    insert_at_existing(z, n, t)
    # print "z intermediate step "+str(z_str(z))
    if z[n] == t:
        # print "move up"
        # Move the others
        for i, j in enumerate(z):
            if j >= z[n] and not i == n:
                z[i] += 1

    if z[n] == t-1:
        # Move the others
        for i, j in enumerate(z):
            if j > z[n] and not i == n:
                # print "invreace layer index by 1 for "+str(i)
                z[i] += 1
        z[n] += 1


def edges_to_node(network, z, n):
    for j in xrange(len(z)):
        if z[i] < z[n]:
            x = randint(0, 1)
            if x == 1:
                network.add_edge(())
        pass


def get_bad_edges(network, z, persons):
        edges = []
        for p in persons:
            node = network.get_nodes()[p]
            for child in network.dag.successors(node):
                ch = child.id
                if z[ch] <= z[p] and (p, ch) not in edges:
                    edges += [(p, ch)]

            for parent in network.dag.predecessors(node):
                pa = parent.id
                if z[pa] >= z[p] and (pa, p) not in edges:
                    edges += [(pa, p)]
        return edges


def prop1(z, network):
    """ Obs!  alpha = 1
    """
    N = len(z)
    K = max(z)+1
    # I) Move to different table
    i = randint(0, N-1)  # Pick random node i
    j = randint(0, K)  # Pick random table j. TODO
    # print "Move node "+str(i) + " from " + str(z[i]) + " to " + str(j)
    (z2, network2) = move(i, j, z, network)  # Move i to j

    # II) Toggle random edge
    a = np.random.randint(N)
    b = np.random.randint(N)
    e = (a, b)
    x = randint(0, 1)
    if not network2.has_edge(e) and z2[a] < z2[b] and x:
        # print "add "+str(e)
        network2.add_edge(e)
    if network2.has_edge(e):
        # print "remove "+str(e)
        remove_edge(z2, network2, e)  # bug!?

    return (z2, network2)


def prop2(z, network, p):
    """ McMC proposal
        Algortihm:
        1. Pick a random layer l1
        2. Pick nodes N with prob p
        3. Pick a random layer l2 (can be new layer)
        4. Move N to l2. (Adds required edges to N)
        5. Add random edges to N with prob 1/2
    """
    K = max(z)+1

    print z_str(z)

    # 1.)
    t1 = np.random.randint(K)
    print "picked layer: " + str(t1)

    # 2.)
    l1 = nodes_in_tables([t1], z)
    print "nodes in the layer are: " + str(l1)
    M = []
    for i, n in enumerate(l1):
        x = np.random.binomial(1, 0.5)
        if x == 1:
            M += [n]

    print "subset to move is: " + str(M)

    # 3.)
    t2 = np.random.randint(K + 1)
    print "move to layer: "+str(t2)

    print "t2  contains: "+str(nodes_in_tables(range(t2), z))

    # 4.) BUG: DOES NOT MOVE
    for i, n in enumerate(M):
        print "moving "+str(n)
        (z2, network2) = move(n, t2, z, network)
        # print "t2 now contains (networj2): "
        # +str(nodes_in_tables(range(t2), z2))
        (z, network) = move(n, t2, z, network)
        print z_str(z)

    print "t2 now conatins: "+str(nodes_in_tables(range(t2), z))
    poss_parents = nodes_in_tables(range(t2), z)
    poss_children = nodes_in_tables(range(t2+1, K), z)
    print "possible_parents: " + str(poss_parents)
    print "possible_children: " + str(poss_children)

    edges = []
    for i, n1 in enumerate(M):
        for j, n2 in enumerate(poss_parents):
            x = np.random.binomial(1, 0.5)
            if x == 1:
                edges += [(n2, n1)]

        for k, n2 in enumerate(poss_children):
            x = np.random.binomial(1, 0.5)
            if x == 1:
                edges += [(n1, n2)]

    print "new edges: " + str(edges)
    network.add_edges(edges)
    return (z, network)


def z_tolist(z):
    z_list = [[] for i in max(z)+1]
    for i, j in z:
        z_list[j].add(i)
    return z_list


def prop3(z1, network1, alpha):
    """
    1) Pick node n.
    2) Assign to existing layer k with prob m_k/(K+alpha).
    and to new with prob alpha/(K+alpha).
    If alone in layer, there will be K possible
    positions, otherwise K+1.
    3) Remove edges contradicting the layers.
    4) Assure edge from z[n]-1 (if not z[n]==0, a)
    5) Assure nodes in z[n]+1 has edges to nodes in z[n].
    6) Fix recursively so that children of n is in proper layer.
    7) Toggle random edge with prob 0.5
    """
    z = list(z1)
    network = network1.copy()
    K = max(z) + 1
    N = len(z)
    m = np.zeros(len(z))
    for i, j in enumerate(z):
        m[j] += 1
    # 1)
    n = randint(0, N-1)
    # print "moving "+str(n)
    children = [p.id for p in network.dag.successors(network.nodes[n])]

    # 2)
    k = randint(0, K + alpha - 1)
    if k >= K:
        if m[z[n]] == 1:
            k = randint(0, K-1)
        else:
            k = randint(0, K)
        # print "NEW table"
        insert_at_new(z, n, k)
    else:
        # print "EXISTING table"
        insert_at_existing(z, n, k)
    # 3)
    bad_edges = get_bad_edges(network, z, [n])
    # print "bad_edges: "+str(bad_edges)
    network.del_edges(bad_edges)
    # 4)
    if z[n] > 0:
        add_random_parent_in_table(n, z[n]-1, z, network)
    # 5)
    if len(nodes_in_tables([z[n]], z)) == 1:
        # print "alone in new table"
        new_children = nodes_in_tables([z[n]+1], z)
        edges = [(n, p) for p in new_children]
        network.add_edges(edges)
    # 6)
    for i in range(len(children)):
        bubble_up(z, network, children[i])

    # 7)
    a = np.random.randint(N)
    b = np.random.randint(N)
    e = (a, b)
    #  if z[a] < z[b] and not a == b:
    if z[a] <= z[b] and not a == b:
        # if not a == b and is_addable(z, network, e):
        x = randint(0, 1)
        if x == 1:
            toggle_edge(network, z, e)
    # if len(get_bad_edges(network, z, [n])) > 0: raise Exception

    return (z, network)


def move(i, j, z1, network1):
    """ Returns a new network and z with i moved to table j.

    """
    z = list(z1)
    network = network1.copy()
    nodes = network.get_nodes()
    K = max(z) + 1

    if j > K:
        print "TO BIG TABLE PROPOSAL"
        return (z, network)  # TODO: return some error here
    # Move i to table j
    z_old = z[i]
    z[i] = j

    # Minimality invariant proposal
    # Check minimal form:
    # 1. If i was the only node in z_old every node below
    #    has to decrease table by one.
    if nodes_in_tables([z_old], z) == []:
        for tid, t in enumerate(z):
            if t > z_old:
                z[tid] -= 1
    # 2. Assure that i and all its former children have some
    # parent in the table above
    n_list = sorted([p.id for p in network.dag.successors(nodes[i])])
    if not j == 0:
        n_list = [i] + n_list

    # print "(node) + children " +str(n_list)
    for n in n_list:
        # check if some parent is in proper table already
        parents = [p.id for p in network.dag.predecessors(nodes[n])]
        # print "parents for "+str(n)
        # print parents
        # check so that we have not moved the only in table 0
        if not z[n] == 0 and not z[n]-1 in [z[p] for p in parents]:
            add_random_parent_in_table(n, z[n]-1, z, network)

    # 4. Redirect contradicting edges
    bad_edges = get_bad_edges(network, z, [i])
    network.reverse_edges(bad_edges)
    # 5. Remove in-group-edges
    bad_edges = get_bad_edges(network, z, [i])
    network.del_edges(bad_edges)
    return (z, network)


def add_random_parent_in_table(i, t, z, network):
    nodes_in_table = nodes_in_tables([t], z)
    p_id = randint(0, len(nodes_in_table)-1)
    parent = nodes_in_table[p_id]
    network.add_edge((parent, i))
    return p_id


def intersect(a, b):
    return list(set(a) & set(b))


def nodes_in_tables(tables, z):
    N = len(z)
    return [p for p in range(N) if z[p] in tables]


def is_addable(z, network, e):
    f = e[0]
    t = e[1]
    t_node = network.nodes[t]
    f_node = network.nodes[f]
    anc = nx.ancestors(network.dag, f_node)
    if f == t:
        return False
    if z[f] < z[t]:
        return True
    if t_node not in anc:
        return True
    return False


def toggle_edge(network, z, e):
    if not network.has_edge(e):
        add_edge(z, network, e)
    else:
        remove_edge(z, network, e)


def add_edge(z, network, e):
    t = e[1]
    if not is_addable(z, network, e):
        print "not addable"
        raise Exception
    network.add_edge(e)
    bubble_down(z, network, t)


def remove_edge(z, network, e):
    network.del_edges([e])
    yid = e[1]
    bubble_up(z, network, yid)


def bubble_down_old(z, network, y_ind):
    """
    Sinks down y_id one layer if its parent is in the same layer.
    This yields z to be the minimal layering of network.
    1. For each parent of the node check if it has higher
    rank then y. If so, return ?
    2. Now, recursively do the same for the children of the node since
    these can now be in the same layer as thier parents.

    Usage: When adding an edge to a network with minimal layering z_
    min use this
    correct z_min if an edges is added to a node in the same layer.

    Args:
    z: "nearly" minimal layering of network.
    """
    y = network.nodes[y_ind]

    parents = network.dag.predecessors(y)
    # print "parents of "+str(y)+ " are "+str(parents)
    for parent in parents:
        p = parent.id
        if z[p] > z[y_ind]:
            return
        if z[p] == z[y_ind]:  # or <= # TODO
            print "bubble down node: "+str(y_ind)
            # z[y_ind] = old_layer + 1
            # NOT +1 , z[p]+1 ? here! make more general! TODO z[p]+1
            z[y_ind] = z[p] + 1
            # recursively correct the children fpr y
            children = network.dag.successors(y)
            for c in children:
                bubble_down_old(z, network, c.id)


def bubble_down(z, network, y_ind):
    """
    Sinks down y_id one layer if its parent is in the same layer.
    This yields z to be the minimal layering of network.
    1. For each parent of the node check if it has higher rank then y.
    If so, return ?
    2. Now, recursively do the same for the children of the node since
    these can now be in the same layer as thier parents.
    Usage: When adding an edge to a network with minimal layering z_min use
    this correct z_min if an edges is added to a node in the same layer.
    Args:
    z: "nearly" minimal layering of network.
    """
    y = network.nodes[y_ind]
    parents = network.dag.predecessors(y)
    # print "parents of "+str(y)+ " are "+str(parents)
    for parent in parents:
        p = parent.id
        # print "parent for y"+str(p)
        if z[p] >= z[y_ind]:  # or <= # TODO
            # print "bubble down node: "+str(y_ind) +" to layer "+str(z[p]+1)
            z[y_ind] = z[p] + 1
            # recursively correct the children fpr y
            children = network.dag.successors(y)
            for c in children:
                bubble_down(z, network, c.id)


def bubble_up(z, network, y_ind):
    """
    Takes a network that where all ancestors to y is in their
    minimal layers given in z.
    The layers are the layers in a hierarchical graph drawing of network.
    Args:
        network: a bayesian network
        y: a node id in network
        z: the layers for the nodes in the network. z =[0,1,1]
           means that node 0 ins in layer 0 and node 1 and 2 is in layer 1.
    """
    # print "bubble up "+str(y_ind)
    # if the node is in its minimal layer
    max_parent_layer = -1
    y = network.nodes[y_ind]
    parents = network.dag.predecessors(y)
    # print "parents of "+str(y)+ " are "+str(parents)
    for parent in parents:
        p = parent.id
        if z[p] > max_parent_layer:
            max_parent_layer = z[p]
        if z[p] == z[y_ind]-1:
            # y had another parent in the layer above.
            return

    # print str(y)+ " not in minl layer."
    # else
    # put y in minimal layer.

    z[y_ind] = max_parent_layer+1
    # print "set "+str(y_ind)+ " in layer "+str(z[y_ind])

    # put the children in their minimal layer.
    children = network.dag.successors(y)
    for c in children:
        bubble_up(z, network, c.id)


def parents_in_tables(i, tables, z, network):
    """
        Returns all the parents of i that are in some of tables
    """
    nodes = network.get_nodes()
    parents = [p.id for p in network.dag.predecessors(nodes[i])]
    return intersect(nodes_in_tables(tables, z), parents)


def get_edges_between_tables(z, network):
    N = len(z)
    edges_between_tables = np.zeros(N*N, dtype=int).reshape(N, N)
    nodes = network.get_nodes()
    for n1 in nodes:
        for child in network.dag.successors(n1):  # Choosing children here
            edges_between_tables[z[n1.id]][z[child.id]] += 1
    return edges_between_tables


def log_prob_graph_given_min_z(z, network, beta_1, beta_2):
    """ The group version of dag_log_density.
        Do not shuffle tables so dont count with rho.
    """
    log_p = 0.0
    K = max(z)+1
    n1 = get_edges_between_tables(z, network)

    for a in range(0, K-1):
        for b in range(a+1, K):
            poss = len(nodes_in_tables([a], z)) * len(nodes_in_tables([b], z))
            n2 = poss - (n1[a, b])  # missing_edges_between_tables
            mb = len(nodes_in_tables([b], z))
            # print "table: "+str(a)," -> ",str(b)
            # print "corr poss: "+str(poss)
            # print "corr existing: "+str(n1[a, b])+ " + "+  str(beta_1[a][b])
            # print "corr miss: "+str(n2) + " + "+ str(beta_2[a][b])
            if b == a+1:
                # print "table: "+str(a)," -> ",str(b)
                # print "corr poss: "+str(poss - mb)
                # print "corr existing: "+str(n1[a, b] - mb)+ "
                # print str(beta_1[a][b])
                # print "corr miss: "+str(n2) + " + "+ str(beta_2[a][b])
                log_p += ss.betaln(beta_1[a][b] + n1[a, b] - mb,
                                   beta_2[a][b] + n2)
            else:
                log_p += ss.betaln(beta_1[a][b] + n1[a][b],
                                   beta_2[a][b] + n2)
            log_p -= ss.betaln(beta_1[a][b], beta_2[a][b])

        # If mode == "single":
        #     existing_edges = len(self.network.dag.edges())
        #     possible_edges = 0

        #     for i in range(K):
        #         for j in range(i,K):
        #             possible_edges += len(self.rest.tables[i].persons) * len(self.rest.tables[j].persons)
        #     missing_edges = possible_edges - existing_edges
        #     log_p = ss.betaln(self.beta_1 + existing_edges, self.beta_2 + possible_edges )
        #     log_p -= ss.betaln(self.beta_1, self.beta_2)
    return log_p


def log_prob_graph_given_z(z, network, beta_1, beta_2):
    """ The group version of dag_log_density.
        Do not shuffle tables so dont count with rho.
    """
    log_p = 0.0
    K = max(z)+1
    n1 = get_edges_between_tables(z, network)
    for a in range(0, K-1):
        for b in range(a+1, K):
            poss = len(nodes_in_tables([a], z)) * len(nodes_in_tables([b], z))
            n2 = poss - n1[a, b]  # missing_edges_between_tables
            log_p += ss.betaln(beta_1[a][b] + n1[a, b], beta_2[a][b] + n2)
            log_p -= ss.betaln(beta_1[a][b], beta_2[a][b])
    return log_p


def dag_log_density(network, z, alpha, beta_1, beta_2):
    log_p = log_prob_z(z, alpha)
    log_p += log_prob_graph_given_min_z(z, network, beta_1, beta_2)
    return log_p


def dag_z_log_density(network, z, alpha, beta_1, beta_2):
    log_p = log_prob_z(z, alpha)
    log_p += log_prob_graph_given_z(z, network, beta_1, beta_2)
    return log_p


def prob_rho_given_z(z):
    """ Calculates the probability ord the order given the person assignment.
    """
    K = max(z) + 1
    return -np.log(sc.factorial(K))  # TODO: change to sum


def log_prob_z(z, alpha):
    """
    Calculates the probability of the state for one permutations
    of the persons.
    """
    K = max(z) + 1
    N = len(z)
    a = alpha
    p = 0
    p += K * np.log(a)
    p += ss.gammaln(a)
    p -= ss.gammaln(N + a)
    for k in range(K):
        m_k = len(nodes_in_tables([k], z))
        p += np.log(sc.factorial(m_k - 1))

    # The marginalization of sigma:
    for k in range(K):
        m_k = len(nodes_in_tables([k], z))
        p += np.log(sc.factorial(m_k))
    p -= np.log(sc.factorial(N))  # prob of perosn assignment

    return p


def exp_K(alpha, n):
    E = 0.0
    for i in range(n):
        E += np.float(alpha) / (alpha + i)
    return E


def get_max_edges_between_tables(m):
    """ This does not take z but m, the number of nodes in each cell.
    """
    K = len(m)
    mat = np.zeros(K*K, dtype=int).reshape(K, K)
    for a in range(K-1):
        for b in range(a+1, K):
            if b == a+1:
                mat[a, b] = (m[a]-1) * m[b]
            else:
                mat[a, b] = m[a] * m[b]
    return mat


def log_p_a(a, alpha):
    """ Log probability for the margianl probability of an allocation
        given in Hoppe.
    """
    n = sum([(i+1)*j for i, j in enumerate(a)])
    log_p = 0.0
    log_p += np.array(range(1, n+1))
    log_p += sum(np.array(range(n)) + alpha)
    log_p += np.log(alpha) * sum(a)
    for i in range(a):
        for j in range(a[i]):
            log_p -= a[i] * np.log(i) * np.log(a[j])
    return log_p


def m_to_a(m):
    pass


def a_to_some_m(a):
    pass


def z_to_m(z):
    pass


def log_multinomial(m):
    """ Calculates the log of the multinomial coefficient.
        Input could e.g. be a partition of integer.
        Args: m defing # elements in each group.
    """
    tot = 0.0
    # Numerator
    n = int(sum(m))
    e = np.array(range(1, n+1))
    log_e = np.log(e)
    tot += sum(log_e)
    # Denominator
    for t in m:
        e = np.array(range(1, int(t)+1))
        log_e = np.log(e)
        tot -= sum(log_e)
    return tot


def log_prob_m(m, alpha):
    K = len(m)
    N = sum(m)
    a = alpha
    p = 0
    p += K * np.log(a)
    p += ss.gammaln(a)
    p -= ss.gammaln(N + a)
    for k in range(K):
        p += np.log(sc.factorial(m[k] - 1))

    # # The marginalization of sigma:
    # for k in range(K):
    #     p += np.log(sc.factorial(m[k]))
    # p -= np.log(sc.factorial(N)) # prob of perosn assignment
    p += log_multinomial(m)
    # TODO: multiply by the number of z giving this m. is this ok?
    # are all really feasible?
    print np.exp(log_multinomial(m))
    return p


def expected_dag_size(n, alpha, beta):
    """ Returns the expected number of edges (graph size)
    for a graph generated with parameters alpha and beta.
    """
    # int_compositions = ruleGen(n, 1, lambda x: 1)
    int_partitions = int_partition(n)
    log_prob = 0.0
    for m in int_partitions:
        print "\nm = "+str(m)
        n_m_matrix = get_max_edges_between_tables(m)
        print n_m_matrix
        # This can be changes by preference
        tot = sum(sum(n_m_matrix))
        print "Max edges = "+str(tot)
        # beta is a matrix of tuples with parameters
        # there we assume they are all equal

        log_E_edges_m = 0.0
        # only
        if len(m) > 1:
            necessary_edges = sum(m[1:])
            # one edge from each table except for first
            log_E_edges_m = np.log(necessary_edges)
        if tot > 0:
            log_E_edges_m += np.log(tot * (beta[0, 1, 0] / (beta[0, 1, 0] + beta[0, 1, 1])))

        log_p_m = log_prob_m(m, alpha)
        print "P(m): "+str(np.exp(log_p_m))
        print "E[size|m]: "+str(np.exp(log_E_edges_m + log_p_m))
        log_prob += log_E_edges_m + log_p_m
    return log_prob


def z_str(z):
    s = []
    K = max(z)+1
    for i in range(K):
        s += [nodes_in_tables([i], z)]
    return s
