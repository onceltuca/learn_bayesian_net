from chinese_rest import *
import networkx as nx
import logging
import time


def proposal1(restgraph):
        """ This is used in th MH algorithm to propose a new (graph,order).

            Args:
                edge_prob: probability of an edge.

            Returns:
                Stats for transition probability.
                This is for this proposal only the id of the person moved.
        """

        # 1. Reassign a random person.
        person_index = np.random.randint(restgraph.rest.no_persons)
        p = restgraph.rest.persons[person_index]
        # old_table = restgraph.rest.reassign_person(person_index)
        bad_edges = restgraph.get_bad_edges([p])

        restgraph.network.reverse_edges(bad_edges)
        same_table_edges = restgraph.get_bad_edges([p])
        restgraph.network.del_edges(same_table_edges)

        # 2. If the person gets in a class of its own, put its table somewhere
        #   at random.
        #   The table with only one person will necessary have ID=len(tables).

        if len(p.table.persons) == 1:
            new_order = np.random.randint(len(restgraph.rest.tables)-1)

            if not new_order == p.table.order:
                restgraph.rest.reorder_table(p.table, new_order)
                bad_edges = restgraph.get_bad_edges([p])
                restgraph.network.reverse_edges(bad_edges)
            else:
                print "ERROR"
                return person_index

        # 3. Pick another person y at randoma and toggle the edge (p,y).
        y = np.random.randint(restgraph.rest.no_persons)
        p2 = restgraph.rest.persons[y]

        if not restgraph.network.has_edge((p.id, y)) and p.table.order < p2.table.order :
            restgraph.network.add_edge((p.id, y))
            return person_index

        if restgraph.network.has_edge((p.id, y)):
            restgraph.network.del_edge((p.id, y))
            return person_index

        # Just in case
        if not restgraph.get_bad_edges(restgraph.rest.persons) == []:
            print restgraph.get_bad_edges(restgraph.rest.persons)
            raise NameError('Network inconsistent with order.')

        # 4. Swap 2 persons.
        # 5. Swap 2 tables.
        # 6. Maybe merge tables.
        # 7. Maybe split tables.
        return person_index


def proposal2(restgraph):
        """ This is used in th MH algorithm to propose a new (graph,order).

            Args:
                edge_prob: probability of an edge.

            Returns:
                Stats for transition probability.
                This is for this proposal only the id of the person moved.
        """
        # 1. Reassign a random person.
        person_index = np.random.randint(restgraph.rest.no_persons)
        p = restgraph.rest.persons[person_index]

        # old_table = restgraph.rest.reassign_person(person_index)  # order ?

        # 2. If the person gets in a class of its own, put its table somewhere
        #   at random.
        #   The table with only one person will necessary have ID=len(tables).
        if len(p.table.persons) == 1:
            new_order = np.random.randint(len(restgraph.rest.tables)-1)
            if not new_order == p.table.order:
                restgraph.rest.reorder_table(p.table, new_order)
                # Delete edges beween nodes as new_order-1 and new_order+1
                prev_table = [t for t in restgraph.rest.tables
                              if t.order == p.table.order-1]

                if not len(prev_table) == 0:
                    edges = []
                    # Find table at prev order
                    for p_id in prev_table[0].persons:
                        node = restgraph.network.get_nodes()[p_id]
                        for child in restgraph.network.dag.successors(node):
                            ch = restgraph.rest.persons[child.id]
                            child_table = ch.table
                            if child_table.order == p.table.order+1 and (p_id, ch.id) not in edges:
                                edges += [(p_id, ch.id)]

                    restgraph.network.del_edges(edges)

        # 1. Delete edges to and from p
        inedges = [(n.id, p.id) for n in restgraph.network.dag.predecessors(restgraph.network.nodes[p.id])]

        restgraph.network.del_edges(inedges)
        out = [(p.id, n.id) for n in restgraph.network.dag.successors(restgraph.network.nodes[p.id])]

        restgraph.network.del_edges(out)

        # 2. Add edges at random
        restgraph.create_edges_from_person(p.id, p.table.order+1, 0.5)
        restgraph.create_edges_to_person(p.id, p.table.order-1, 0.5)

        return person_index


def proposal3(restgraph):
        """ A proposal on DAGs that will give make the chain irreduciible
        """
        pass


def metropolis_hastings(iterations, start_restgraph, alpha, alpha_mcmc):
    """ TODO: save acceptance rate.
    """
    graphs = []

    no_acc = 0.0
    restgraphs = []
    restgraph = start_restgraph
    restgraphs.append(restgraph)
    accept = 0
    prob_g1 = 0
    logging.info("new chain")
    for i in range(1, iterations):
        logging.info(str(i)+"/"+str(iterations))
        current_restgraph = restgraphs[i-1]
        proposed_restgraph = restgraphs[i-1].copy()

        moved_person_id = proposal1(proposed_restgraph)

        logging.debug("Old restgraph \n "+str(current_restgraph))
        logging.debug("Moved: "+str(moved_person_id))
        logging.debug("Proposed restgraph \n" + str(proposed_restgraph))

        # AD-HOC
        restgraphs[i-1].rest.concentration = alpha_mcmc
        proposed_restgraph.rest.concentration = alpha_mcmc
        prob_g1 = restgraphs[i-1].prob()
        prob_g2 = proposed_restgraph.prob()
        restgraphs[i-1].rest.concentration = alpha
        proposed_restgraph.rest.concentration = alpha

        ratio = get_transition_probs(proposed_restgraph.rest,
                                     current_restgraph.rest,
                                     moved_person_id)
        ratio /= get_transition_probs(current_restgraph.rest,
                                      proposed_restgraph.rest,
                                      moved_person_id)

        accept_prob = min(1.0, np.exp(prob_g2-prob_g1) * ratio)
        accept = np.random.binomial(1, accept_prob)

        if accept:
            logging.debug("ACC (JUMP)")
            restgraphs.append(proposed_restgraph)
            graphs.append
            no_acc += 1
        else:
            logging.debug("REJECT (STAY)")
            restgraphs.append(restgraphs[i-1])

    logging.info("Acceptance rate: "+str(no_acc / iterations))
    return restgraphs


def get_transition_rest(rest1, rest2, moved_person_id):
    """ MCMC transition prob
    """
    t1 = rest1.persons[moved_person_id].table
    t2 = rest2.persons[moved_person_id].table
    tables = [t1, t2]
    alpha = np.float(rest1.concentration)
    n = rest1.no_persons
    N = np.float(n)

    # k['j'] is number of person in tabls after removing person i
    # and before reinserting it.
    k = {'i': np.float(len(tables[0].persons)),
         'j': np.float(len(tables[1].persons) - 1)}
    K = {'1': np.float(len(rest1.tables)),
         '2': np.float(len(rest2.tables))}
    # Prob  move z
    # 1. Prob go to itself
    if tables[0].persons == tables[1].persons:
        q = 0.0
        m = 0.0  # number of tables with >1 person
        for t in rest1.tables:
            kj = np.float(len(t.persons))
            if kj > 1:
                q += (kj / N) * ((kj - 1) / (N-1+alpha))
            else:
                m += 1
        q += (m/N) * (alpha / (N-1+alpha)) * (1/K['1'])
        return q

    if k['j'] == 0:
        # Moved to a new cluster
        q = (k['i']/N) * (alpha / (alpha + (N-1))) * (1 / K['2'])
        return q
    else:
        # To an existing cluser
        q = (k['i']/N) * (k['j']/(alpha + (N-1)))
        return q


def get_graph_trans(to_rest, moved_person_id):
    # 2. Prob move graph
    N = len(to_rest.persons)
    k = len(to_rest.persons[moved_person_id].table.persons)

    if k == N:
        # z_2 has only onle class
        return 1.0
    else:
        return (1.0 / np.float(N - k))


def get_transition_probs(rest1, rest2, moved_person_id):
    ret = get_transition_rest(rest1, rest2, moved_person_id)
    ret *= get_graph_trans(rest2, moved_person_id)
    return ret


def create_chain(data, iterations, alpha, beta_1, beta_2,
                 alpha_mcmc, mode, support, prior, gamma, cache=None):
    """
    Create Markov chains
    """
    logging.debug("MCMC")
    n = len(data[0])

    # Initialize
    r = Restaurant(alpha, n, 666)
    r.assign_persons()
    rg = RestGraph(r, beta_1, beta_2, mode)
    rg.set_edge_prob(mode)
    rg.create_network(data, support, prior, gamma)

    # if i > 0:
    #    rg.network.diff_cache = restgraphs[i-1][0].network.diff_cache
    if cache is not None:
        rg.network.diff_cache = cache

    start = time.clock()
    restgraphs = metropolis_hastings(iterations, rg, alpha, alpha_mcmc)
    stop = time.clock()

    cpu_time = stop - start
    zs = [rg1.rest.get_z() for rg1 in restgraphs]
    coopers = [rg1.network.cooper_likelihood for rg1 in restgraphs]
    probs = [rg1.prob() for rg1 in restgraphs]
    dags = [np.array(rg1.network.get_numpy_matrix().flatten())[0]
            for rg1 in restgraphs]

    return (zs, coopers, probs, dags, cpu_time)


def pool(data, no_chains, iterations, alpha, beta_1,
         beta_2, alpha_mcmc, mode, support, prior, gamma, path):
    """ Get a pool of MCMC chains """
    n = len(support)
    zs_list = []
    cooper_list = []
    prob_list = []
    dags_list = []
    cputimes_list = []
    cache = None
    for i in range(no_chains):
        (zs, coopers, probs, dags, cpu_time) = create_chain(data,
                                                            iterations,
                                                            alpha,
                                                            beta_1,
                                                            beta_2,
                                                            alpha_mcmc,
                                                            mode,
                                                            support,
                                                            prior,
                                                            gamma,
                                                            cache)

        logging.debug("Writing files")
        # TODO: fix cache
        # cache = restgraphs[i-1][0].network.diff_cache
        np.savetxt(path+'/mcmc_dags_'+str(i)+'.txt', dags, fmt='%.1i')
        np.savetxt(path+'/mcmc_zs_'+str(i)+'.txt', zs, fmt='%.1i')
        np.savetxt(path+'/mcmc_cooper_'+str(i)+'.txt', coopers, fmt='%10.15f')
        np.savetxt(path+'/mcmc_prob_'+str(i)+'.txt', probs, fmt='%10.15f')
        np.savetxt(path+'/mcmc_cputimes_'+str(i)+'.txt', [cpu_time],
                   fmt='%10.15f')

        ds = [nx.DiGraph(np.matrix(d).reshape(n, n)) for d in dags]

        cputimes_list.append(cpu_time)
        zs_list.append(zs)
        cooper_list.append(coopers)
        prob_list.append(probs)
        dags_list.append(ds)

    return (zs_list, cooper_list, prob_list, dags_list, cputimes_list)
